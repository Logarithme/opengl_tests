HOW TO BUILD

Compiling externals
-> glfw-3.0.4

cd externals
unzip glfw-3.0.4.zip
cd glfw-3.0.4
mkdir build
cd build
cmake -D BUILD_SHARED_LIBS=1 ..
make


-> AntTweakBar

cd externals
unzip AntTweakBar_116.zip
cd AntTweakBar
cd src
make


-> CImg

cd externals
unzip CImg_latest.zip
mv CImg-1.6.5_pre070815 CImg-1.6.5


-> Assimp

cd externals
unzip assimp-3.1.1_no_test_models.zip
cd assimp-3.1.1/
mkdir build
cd build
cmake .. -G 'Unix Makefiles'
make


