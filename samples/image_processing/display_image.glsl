#version 330

$start VERTEX_SHADER

layout (location=0) in vec3 position;

out vec4 vertex_position;
out vec2 vertex_texcoord;

void main()
{	
	vertex_position = vec4(position, 1.0);
	vertex_texcoord = (position.xy+vec2(1, 1))*0.5;
	gl_Position = vertex_position;
}

$end VERTEX_SHADER

$start FRAGMENT_SHADER

uniform sampler2D img;

in vec2 vertex_texcoord;
in vec4 vertex_position;
out vec4 color;

void main()
{
    color = texture(img, vertex_texcoord);
}

$end FRAGMENT_SHADER