/**
* Load and display a mesh
**/

#include "../../src/window.h"
#include "../../src/texture.h"
#include "../../src/program.h"
#include "../../src/geo_math.h"
#include "../../src/misc.h"
#include "../../src/buffer.h"
#include "../../src/camera.h"
#include "../../src/mesh.h"

#include <cstdio>
#include <cstdlib>

#define SHADER_PATH(x) "../samples/image_processing/" x

int main(int argc, char** argv)
{
    if (argc <= 1)
    {
        printf("\n\n Usage %s <image_file>\n \n\n", argv[0]);
		return 0;
    }
    
    GLWindow* w = new GLWindow(true, true);
    glEnable(GL_DEPTH_TEST);
	
	int nb_particles = 10;
	
	SSBO ssbo;
	ssbo.setData(NULL, nb_particles, sizeof(Vector3), GL_DYNAMIC_DRAW);
	
	Vector3* shared_pos = (Vector3*)ssbo.mapToCPU('x');
	for(int i=0; i<nb_particles; i++)
		shared_pos[i] = Vector3( ((float)i/(float)nb_particles), 0, 0 );
	ssbo.unmap();
	
	Program* p = makeProgramFrom((char*)SHADER_PATH("ssbo_sample.glsl"));
	
	Camera c;
	c.setTo(Vector3(0, 0, -1));
	
	Mesh m;
	m.loadEmptyVertexArray(nb_particles);
	
    do
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(1, 1, 1, 1);
		
		ssbo.bind(1);
		
		p->setUniform("mvp", w->getMatrix() * c.getMatrix());
		
		p->use();
	
		m.bindIt();
		glPointSize(10);
		m.drawMesh(GL_POINTS);
		m.unbindIt();
		
		p->unuse();
		
        w->swapAndPoll();
		
		c.moveWithKeyboard(w, 0.1);
        
        // Events handling
		if (w->isKeyPressedOnce(GLFW_KEY_C))
			capture("", 1024, 768);
	}
    while(!(w->isKeyPressed(GLFW_KEY_ESCAPE)) && !(w->isClosed()));
    
    //FREE
    delete p;
	
    return 0;
}
