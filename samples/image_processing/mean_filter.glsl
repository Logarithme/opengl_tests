#version 430 core

$start COMPUTE_SHADER

layout(binding=0, rgba8) writeonly uniform image2D result;

layout(local_size_x = 1, local_size_y = 1) in;

void main( )
{
    imageStore(result, ivec2(gl_GlobalInvocationID.xy), vec4(255, 0, 0, 1));
}

$end COMPUTE_SHADER