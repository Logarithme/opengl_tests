#version 430

$start VERTEX_SHADER

uniform mat4 mvp;
float nb_part = 10;

struct tototo
{
	float x;
	float y;
	float z;
};

layout (std430, binding=1) buffer shader_data
{
	tototo position[];
};

out flat int id;

void main()
{
	id = gl_VertexID;
	gl_Position = mvp * vec4( position[gl_VertexID].x, 0, 0, 1 );
}

$end VERTEX_SHADER

$start FRAGMENT_SHADER

float nb_part = 10;

in flat int id;

out vec4 color;

void main()
{
	color = vec4((id+1)/nb_part, 0, 0, 1);
}

$end FRAGMENT_SHADER