#version 330

$start VERTEX_SHADER

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

layout (location=0) in vec3 position;
layout (location=2) in vec3 normal;

out vec3 v_normal;
out vec3 c_direction;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1);
   
    vec4 vertex_position = view * model * vec4(position, 1); //cameraspace
    c_direction = -vertex_position.xyz; //cameraspace
    
    v_normal = (view * model * vec4(normal, 0)).xyz; //cameraspace
}

$end VERTEX_SHADER

$start FRAGMENT_SHADER

uniform vec4 m_color;
uniform vec3 light_dir;
uniform float light_power;
uniform float alpha;
uniform float beta;
uniform float ph_shine;

in vec3 v_normal;
in vec3 c_direction;

out vec4 color;

void main()
{	
    vec4 tex_color = m_color;
    float l_dist = 1;

    vec3 k = vec3(1, 1, 1);
    
    //ambiant_lighting
    color = k.x * vec4( 0.5*m_color.xyz, m_color.a); 
    
    //diffuse_lighting
    float NL = dot(normalize(v_normal), normalize(light_dir));
    float NL_clamp = clamp(dot(normalize(v_normal), normalize(light_dir)), 0, 1);
    color += k.y * (m_color * NL_clamp * light_power / (l_dist * l_dist));
    
    //gooch shading
    float kd = 1;

    float it = ((1 + NL) / 2);
    vec3 tmp_color = (1-it) * (vec3(0, 0, 0.5) + alpha*m_color.xyz) +  it * (vec3(0.5, 0.5, 0) + beta*m_color.xyz);

    //Highlights
    vec3 R = reflect(-normalize(light_dir), normalize(v_normal));
    float ER = clamp(dot(normalize(c_direction), normalize(R)), 0, 1);

    vec4 spec = vec4(1) * pow(ER, ph_shine);

    color = vec4(tmp_color+spec.xyz, m_color.a);
}
$end FRAGMENT_SHADER