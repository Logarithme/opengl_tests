/**
* Load and display a mesh
**/

#include "../../src/window.h"
#include "../../src/mesh.h"
#include "../../src/program.h"
#include "../../src/geo_math.h"
#include "../../src/texture.h"
#include "../../src/misc.h"
#include "../../src/camera.h"
#include "../../src/framebuffer.h"
#include "../../src/scene.h"

#include <cstdio>
#include <cstdlib>

#define SHADER_PATH(x) "../samples/mesh_viewer/" x

typedef enum { GOOCH=0, PHONG=1, WARD=2, DIFFUSE=3, GOOCH_SHADOW=4, CELL=5, SATURATED=6, NUM_SHADING=7 } Shading;

Shading current_shade = CELL;
    
float color[] = {1, 0, 0, 1};
float silhouette_color[] = {0, 0, 0, 1};
double light_dir[3] = {-1, -1, 0}; // direction pointing to +x and +y
float light_power = 1;

bool textured = false;
bool draw_silhouette = false;
bool draw_object = true;
bool wireframe = false;

float phong_shine = 10;
float ward_anisot = 0.5;
float ward_anisob = 0.5;
float gooch_alpha = 0.2;
float gooch_beta = 0.6;
float gooch_shadow_p = 0.78;
float gooch_shadow_alpha = 0.76;
float saturated_shadow_color[] = {0, 0, 0, 1};

Texture3D createCelTexture()
{
	int w, h, d;

	w=h=d=4;

	unsigned char* data = (unsigned char*)malloc(sizeof(unsigned char)*w*h*d*3);
	for(int i=0; i<w; i++)
	for(int j=0; j<h; j++)
	for(int k=0; k<d; k++)
	{
		int pos = (((k*h +j)*d)+i)*3;
		data[pos] = (unsigned char)(i*(1.0/(float)(w-1))*255);
		data[pos+1] = (unsigned char)(j*(1.0/(float)(h-1))*255);
		data[pos+2] = (unsigned char)(k*(1.0/(float)(d-1))*255);
	}
		
	Texture3D t;
	t.loadTextureData(data, w, h, d, GL_UNSIGNED_BYTE, 3, 0, 0);

	return t;
}


void loadShaders(Program** shaders)
{
	shaders[GOOCH] = makeProgramFrom((char*)SHADER_PATH("gooch.glsl"));
	shaders[WARD] = makeProgramFrom((char*)SHADER_PATH("ward.glsl"));
	shaders[PHONG] = makeProgramFrom((char*)SHADER_PATH("phong.glsl"));
	shaders[DIFFUSE] = makeProgramFrom((char*)SHADER_PATH("diffuse.glsl"));
	shaders[GOOCH_SHADOW] = makeProgramFrom((char*)SHADER_PATH("gooch2.glsl"));
	shaders[CELL] = makeProgramFrom((char*)SHADER_PATH("cell.glsl"));
	shaders[SATURATED] = makeProgramFrom((char*)SHADER_PATH("saturated.glsl"));
}

void loadInterface()
{
	TwBar *bar = TwNewBar("Shading");

	TwAddVarRW(bar, "LightDir", TW_TYPE_DIR3D, &light_dir, "label='Light direction' help='Changes the incoming light direction.'");
	TwAddVarRW(bar, "LightPower", TW_TYPE_FLOAT, &light_power, "min=0.1 max=10 step=0.1 label='Light Power' help='Change the emmiting power of the light' ");
	TwAddVarRW(bar, "Color", TW_TYPE_COLOR4F, &color, " label='Mesh color' alpha help='Color and transparency of the mesh.' ");
	TwAddVarRW(bar, "Silhouette Color", TW_TYPE_COLOR4F, &silhouette_color, " label='Silhouette color' alpha help='Color and transparency of the silhouette.' ");
	TwAddVarRW(bar, "Silhouette", TW_TYPE_BOOLCPP, &draw_silhouette, " label='Silhouette' help='Drawing edges of the mesh.' ");
	TwAddVarRW(bar, "Object", TW_TYPE_BOOLCPP, &draw_object, " label='Mesh' help='Drawing the mesh.' ");
	TwAddVarRW(bar, "Wireframe", TW_TYPE_BOOLCPP, &wireframe, " label='Wireframe' help='Drawing mesh in wireframe mode.' ");
	TwAddVarRW(bar, "Textured", TW_TYPE_BOOLCPP, &textured, " label='Textured' help='Is the mesh displayed with a texture.' ");
	{
		TwEnumVal shapeEV[NUM_SHADING] = { 
			{GOOCH, "Gooch"}, 
			{PHONG, "Phong"}, 
			{WARD, "Ward"}, 
			{DIFFUSE, "Diffuse"}, 
			{GOOCH_SHADOW, "Gooch Shadow"}, 
			{CELL, "Cell shading"},
			{SATURATED, "Saturated"}
		};
		TwType shadeType = TwDefineEnum("ShapeType", shapeEV, NUM_SHADING);
		TwAddVarRW(bar, "Shading", shadeType, &current_shade, " help='Change shading.' ");
	}
	//Phong
	TwAddVarRW(bar, "Shine", TW_TYPE_FLOAT, &phong_shine, " min=0 max=200 step=1 label='Shine' help='Change the shininess of the material' group='Phong' ");
	//Ward
	TwAddVarRW(bar, "AnisotroT", TW_TYPE_FLOAT, &ward_anisot, " min=0 max=1 step=0.01 label='Aniso-T' help='Change the anisotropy of the material' group='Ward' ");
	TwAddVarRW(bar, "AnisotroB", TW_TYPE_FLOAT, &ward_anisob, " min=0 max=1 step=0.01 label='Aniso-B' help='Change the anisotropy of the material' group='Ward' ");
	//Gooch
	TwAddVarRW(bar, "Cool color", TW_TYPE_FLOAT, &gooch_alpha, " min=0 max=1 step=0.05 label='Cool factor' help='Change the amount of mesh color in the cool side of the shading' group='Gooch' ");
	TwAddVarRW(bar, "Warm color", TW_TYPE_FLOAT, &gooch_beta, " min=0 max=1 step=0.05 label='Warm factor' help='Change the amount of mesh color of warm side of the shading cooling' group='Gooch' ");
	//Gooch shadow
	TwAddVarRW(bar, "Shadow Alpha", TW_TYPE_FLOAT, &gooch_shadow_alpha, " min=0 max=1 step=0.05 label='Shadow size' help='Change the size of the shadow' group='Gooch Shadow' ");
	TwAddVarRW(bar, "Shadow P", TW_TYPE_FLOAT, &gooch_shadow_p, " min=0 max=1 step=0.05 label='Shadow darkness' help='Change the darkness of the shadow' group='Gooch Shadow' ");
	//Saturated
	TwAddVarRW(bar, "Saturation Color", TW_TYPE_COLOR4F, &saturated_shadow_color, " label='Shadow color' alpha help='Color of the saturated shadow.' group='Saturated' ");
}

int main(int argc, char** argv)
{
    
	if (argc <= 1)
	{
		printf("\n\n Usage %s <mesh_file> [texture_file]\n \n\n", argv[0]);
		return 0;
	}

	GLWindow* w = new GLWindow(false, true);

	Mesh m(argv[1]);

	Scene s;
	s.loadFromFile(argv[1]);

	Texture2D t;
	if (argc > 2)
	{
		t.loadTexture(argv[2], GL_UNSIGNED_BYTE, 1);
		t.generateMipmap();
		textured = true;
	}

	Texture3D cell_t = createCelTexture();
	cell_t.generateMipmap();

	Program** shaders = (Program**)malloc(sizeof(Program*)*NUM_SHADING);
	loadShaders(shaders);
	Program* glsl_silhouette = makeProgramFrom((char*)SHADER_PATH("mesh_viewer_silhouette.glsl"));

	Camera* c = new Camera();
	Vector3 cpos = Vector3(0, 0, -2.5*s.box().getSizes().z()) + s.box().getMidPoint();
	c->setTo(Vector3(cpos.x(), cpos.y()*-1, cpos.z()));
	
	//Silhouette utils
	Mesh screen;
	screen.loadFullSquare();
	
	Texture2D t_fbo;
	t_fbo.loadTextureData(NULL, w->getWidth(), w->getHeight(), GL_UNSIGNED_BYTE, 3, 0, 0);
	
	Framebuffer fbo;
	fbo.attachDepthRBO();
	fbo.attachColorTexture(t_fbo);
	
	loadInterface();

	glEnable(GL_DEPTH_TEST);

	do
	{
		glClearColor(1, 1, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		t.activateTexture();
		cell_t.activateTexture();
			
		/*glsl_silhouette->setUniform("view", c->getMatrix());
		glsl_silhouette->setUniform("projection", w->getMatrix());*/
		glsl_silhouette->setUniform("image", t_fbo.getId() );
		glsl_silhouette->setUniform("color", Vector4(silhouette_color));

		for(int i=0; i<NUM_SHADING; i++)
		{
			shaders[i]->setUniform("view", c->getMatrix());
			shaders[i]->setUniform("projection", w->getMatrix());
			shaders[i]->setUniform("m_color", Vector4(color));
			shaders[i]->setUniform("light_dir", Vector3(light_dir) * -1);
			shaders[i]->setUniform("light_power", light_power);
			//Uniforms that are specific to some shaders but are loaded for everyone to increase readability
			shaders[i]->setUniform("textured", textured);
			shaders[i]->setUniform("tex", t.getId());
			shaders[i]->setUniform("ph_shine", phong_shine);
		}
	
		shaders[WARD]->setUniform("wd_aniT", ward_anisot);
		shaders[WARD]->setUniform("wd_aniB", ward_anisob);
		
		shaders[GOOCH]->setUniform("ph_shine", phong_shine);
		shaders[GOOCH]->setUniform("alpha", gooch_alpha);
		shaders[GOOCH]->setUniform("beta", gooch_beta);
		
		shaders[GOOCH_SHADOW]->setUniform("ph_shine", phong_shine);
		shaders[GOOCH_SHADOW]->setUniform("alpha", gooch_alpha);
		shaders[GOOCH_SHADOW]->setUniform("beta", gooch_beta);
		shaders[GOOCH_SHADOW]->setUniform("p_shadow", gooch_shadow_p);
		shaders[GOOCH_SHADOW]->setUniform("alpha_shadow", gooch_shadow_alpha);
		
		shaders[CELL]->setUniform("texture_cell", cell_t.getId());
		
		shaders[SATURATED]->setUniform("shadow_color", Vector4(saturated_shadow_color));
	
		// Draw
		/*
		if (draw_silhouette)
		{
			glEnable(GL_CULL_FACE);
			glCullFace (GL_FRONT);
			
			glDepthFunc (GL_LEQUAL);
			
			glPolygonMode (GL_FRONT_AND_BACK, GL_LINE); 
			
			glsl_silhouette->use();
			
			glLineWidth(4);
			
			for(unsigned int i=0; i<s.getNumMeshes(); i++)
			{
				Mesh* m = &(s.getMeshesList()[i]);
				glsl_silhouette->setUniform("model", m->getMatrix());
				for(int i=0; i<NUM_SHADING; i++)
					shaders[i]->setUniform("model", m->getMatrix());
			
				m->bindIt();
				m->drawMesh(GL_TRIANGLES);
				m->unbindIt();
			}
	
			glLineWidth(1);
			
			glsl_silhouette->unuse();
			
			glCullFace (GL_BACK);
			glDisable(GL_CULL_FACE);
			
			glDepthFunc (GL_LESS);
		
		}
		*/
		
		if (wireframe)
			glPolygonMode (GL_FRONT_AND_BACK, GL_LINE); 
		else 
			glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		
		fbo.set();
		
		glClearColor(1, 1, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		if (draw_object)
		{
			shaders[current_shade]->use();
			
			for(unsigned int i=0; i<s.getNumMeshes(); i++)
			{
				Mesh* m = &(s.getMeshesList()[i]);
				for(int i=0; i<NUM_SHADING; i++)
					shaders[i]->setUniform("model", m->getMatrix());
			
				m->bindIt();
				m->drawMesh(GL_TRIANGLES);
				m->unbindIt();
			}
			
			shaders[current_shade]->unuse();
		}
		
		fbo.unset();
		
		t_fbo.activateTexture();
		t_fbo.generateMipmap();
		
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		
		glsl_silhouette->use();
		
		screen.bindIt();
		screen.drawMesh(GL_TRIANGLE_STRIP);
		screen.unbindIt();
		
		glsl_silhouette->unuse();

		// Events handling
		w->swapAndPoll();
		
		/**Camera**/
		float speed = s.box().getSizes().z() * 0.001;
		c->moveWithKeyboard(w, speed);

		/**Object**/
		s.rotateOnMouse(w);
			
		if (w->isKeyPressed(GLFW_KEY_R))
		{
			for(int i=0; i<NUM_SHADING; i++)
			shaders[i]->reload();
			glsl_silhouette->reload();
		}
	
		if (w->isKeyPressedOnce(GLFW_KEY_C))
			capture("", 1024, 768);
    }
	while(!(w->isKeyPressed(GLFW_KEY_ESCAPE)) && !(w->isClosed()));

	return 0;
}
