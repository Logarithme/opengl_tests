#version 330

$start VERTEX_SHADER

/*
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


layout (location=0) in vec3 position;
layout (location=2) in vec3 normal;
layout (location=3) in vec2 texcoord;

void main()
{
    vec3 p = position + 0.03*normalize(normal);
    gl_Position = projection * view * model * vec4(p, 1);
}
*/

layout (location=0) in vec3 position;

out vec2 tex_coord;

void main()
{
	tex_coord = (position.xy + 1)*0.5;
    gl_Position = vec4(position, 1);
}

$end VERTEX_SHADER

$start FRAGMENT_SHADER

/*
uniform vec4 color;

layout(location=0) out vec4 f_color;

void main()
{
	f_color = color;
}
*/
uniform sampler2D image;
uniform vec4 color;

in vec2 tex_coord;

out vec4 f_color;

void main()
{
	int filter_size = 3;
	
	vec4 gradient_x = vec4(0);
	mat3 filter_x = mat3 (vec3(-1, 0, 1), vec3(-2, 0, 2), vec3(-1, 0, 1));
	for(int i=-(filter_size/2); i<=(filter_size/2); i++)
	for(int j=-(filter_size/2); j<=(filter_size/2); j++)
	{
		gradient_x += filter_x[i+(filter_size/2)][j+(filter_size/2)] * texelFetch(image, ivec2( tex_coord*vec2(1024, 768)) + ivec2(i, j), 0);
	}
	
	vec4 gradient_y = vec4(0);
	mat3 filter_y = mat3 (vec3(-1, -2, -1), vec3(0, 0, 0), vec3(1, 2, 1));
	for(int i=-(filter_size/2); i<=(filter_size/2); i++)
	for(int j=-(filter_size/2); j<=(filter_size/2); j++)
	{
		gradient_y += filter_y[i+(filter_size/2)][j+(filter_size/2)] * texelFetch(image, ivec2( tex_coord*vec2(1024, 768)) + ivec2(i, j), 0);
	}
		
	//f_color = texture(image, tex_coord);
	if(length(gradient_x + gradient_y) > 0.5)
		f_color = color;
	else f_color = vec4(1);
}

$end FRAGMENT_SHADER
