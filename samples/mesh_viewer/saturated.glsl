#version 330

$start VERTEX_SHADER

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

layout (location=0) in vec3 position;
layout (location=2) in vec3 normal;
layout (location=3) in vec2 texcoord;

out vec3 v_normal;
out vec3 c_direction;
out vec2 v_texcoord;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1);
   
    vec4 vertex_position = view * model * vec4(position, 1); //cameraspace
    c_direction = -vertex_position.xyz; //cameraspace
    
    v_normal = (view * model * vec4(normal, 0)).xyz; //cameraspace
    
    v_texcoord = texcoord;
}

$end VERTEX_SHADER

$start FRAGMENT_SHADER

uniform vec4 m_color;
uniform vec3 light_dir;
uniform float light_power;
uniform vec4 shadow_color;

uniform int textured;
uniform sampler2D tex;

in vec3 v_normal;
in vec3 c_direction;
in vec2 v_texcoord;

out vec4 color;

void main()
{	
    vec4 tex_color = m_color;
    if (textured == 1)
	tex_color = texture(tex, v_texcoord);
    float l_dist = 1;

    vec3 k = vec3(1, 1, 1);
    
    //diffuse_lighting
    float NL = dot(normalize(v_normal), normalize(light_dir));
    float NL_clamp = clamp(dot(normalize(v_normal), normalize(light_dir)), 0, 1);
    
    if (NL_clamp != 0)
		color = vec4(1);
	else
		color = shadow_color;
}
$end FRAGMENT_SHADER