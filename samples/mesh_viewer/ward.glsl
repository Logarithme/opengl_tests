#version 330

$start VERTEX_SHADER

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

layout (location=0) in vec3 position;
layout (location=2) in vec3 normal;
layout (location=3) in vec2 texcoord;
layout (location=4) in vec3 tangente;
layout (location=5) in vec3 bitan;

out vec3 v_normal;
out vec3 v_tan;
out vec3 v_bitan;
out vec3 c_direction;
out vec2 v_texcoord;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1);
   
    vec4 vertex_position = view * model * vec4(position, 1); //cameraspace
    c_direction = -vertex_position.xyz; //cameraspace
    
    v_normal = (view * model * vec4(normal, 0)).xyz; //cameraspace
    v_tan = (view * model * vec4(tangente, 0)).xyz;
    v_bitan = (view * model * vec4(bitan, 0)).xyz;
    
    v_texcoord = texcoord;
}

$end VERTEX_SHADER

$start FRAGMENT_SHADER

vec4 ambiant_lighting(vec4 m_color)
{
	return vec4( 0.5*m_color.xyz, m_color.a);
}

vec4 diffuse_lighting(vec4 m_color, float l_power, float l_dist, vec3 l_direction,  vec3 v_normal)
{
	float NL = clamp(dot(normalize(v_normal), normalize(l_direction)), 0, 1);
	
	vec4 color = m_color * NL * l_power / (l_dist * l_dist);
	
	return color;
}

vec4 ward_spec_lighting(vec4 m_color, vec3 l_direction, vec3 v_normal, vec3 v_tan, vec3 v_bitan, vec3 c_direction, float sigT, float sigB)
{
	float dotNL = clamp( dot(normalize(v_normal), normalize(l_direction)), 0, 1 ); 
	float dotNV = clamp( dot(normalize(v_normal), normalize(c_direction)), 0, 1 );

	vec3 H = (normalize(l_direction) + normalize(c_direction)) / 2.0;
	
	float dotHT = dot(normalize(H), normalize(v_tan));
	float dotHB = dot(normalize(H), normalize(v_bitan));
	float dotNH = clamp( dot(normalize(v_normal), normalize(H)), 0, 1);
	
	float norm_factor = 4 * 3.14159 * sigT * sigB;
	norm_factor *= sqrt(dotNL * dotNV);
	
	if (dotNL != 0 && dotNV != 0)
		norm_factor = 1.0 / norm_factor;
	
	float dotHT2 = dotHT*dotHT;
	float dotHB2 = dotHB*dotHB;
	
	float sigT2 = sigT*sigT;
	float sigB2 = sigB*sigB;
	
	float exp_factor = ( dotHT2 / sigT2 ) + ( dotHB2 / sigB2 );
	exp_factor /= (1 + dotNH);
	exp_factor *= -2;
	
	/*float exp_factor = dotNH*dotNH - 1;
	exp_factor /= (dotNH * sigT*sigT);*/
	
	return m_color * exp(exp_factor) * norm_factor;
}

uniform int textured;
uniform vec4 m_color;
uniform vec3 light_dir;
uniform float light_power;

uniform float wd_aniT;
uniform float wd_aniB;

uniform sampler2D tex;

in vec3 v_normal;
in vec3 v_tan;
in vec3 v_bitan;
in vec3 c_direction;
in vec3 l_direction;
in vec2 v_texcoord;

out vec4 color;

void main()
{	
    vec4 m_spec_color = vec4(1);
    
    vec4 tex_color = m_color;
    if (textured == 1)
	tex_color = texture(tex, v_texcoord);
    
    vec3 k = vec3(1, 1, 1);
    color = k.x * ambiant_lighting(tex_color);
    color += k.y * diffuse_lighting(tex_color, light_power, 1, light_dir, v_normal);
    color += k.z * ward_spec_lighting(m_spec_color, light_dir, v_normal, v_tan, v_bitan, c_direction, wd_aniT, wd_aniB);
	
}
$end FRAGMENT_SHADER