#version 330

$start VERTEX_SHADER

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

uniform float u_size;
uniform vec3 u_center;

layout (location=0) in vec3 position;

void main()
{
    vec3 transformed_pos = position * u_size;
    transformed_pos += u_center;

    gl_Position = projection * view * model * vec4(transformed_pos, 1);
}

$end VERTEX_SHADER

$start FRAGMENT_SHADER

uniform vec3 u_color;

out vec4 color;

void main()
{	
   color = vec4(u_color, 1);
}
$end FRAGMENT_SHADER