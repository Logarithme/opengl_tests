/**
* Load and display a mesh
**/

#include "../../src/window.h"
#include "../../src/mesh.h"
#include "../../src/program.h"
#include "../../src/geo_math.h"
#include "../../src/texture.h"
#include "../../src/misc.h"
#include "../../src/camera.h"
#include "../../src/framebuffer.h"
#include "../../src/scene.h"

#include <cstdio>
#include <cstdlib>

#define SHADER_PATH(x) "../samples/mipmap_sphere/" x

typedef Vector3 vec3;

Vector3 symetries[48];

Vector3 colors[2000];
Vector3 positions[2000];
float sizes[2000];
int nb_cubes = 0;

void draw_mesh(Mesh* m, Program* p, Vector3 color, float size, Vector3 center=Vector3(0, 0, 0))
{
	p->setUniform("u_color", color);
	p->setUniform("u_size", size);
	p->setUniform("u_center", center);
	
	m->bindIt();
	m->drawMesh(GL_TRIANGLES);
	m->unbindIt();
}

void fillSymetries48()
{
	int id = 0;
	for(int i=1; i<=3; i++)
	{
		for(int j=1; j<=3; j++)
		{
			if(j==i)
				continue;
			for(int k=1; k<=3; k++)
			{
				if (k==i || k== j)
					continue;
				
				for(int s0=1; s0>=-1; s0-=2)
				for(int s1=1; s1>=-1; s1-=2)
				for(int s2=1; s2>=-1; s2-=2)
					symetries[id++]=Vector3(s0*i, s1*j, s2*k);
			}
		}
	}
	
	
	printf("Loaded %d symetries\n", id);
}

void fillSymetries24()
{
	int id = 0;
	for(int i=1; i<=3; i++)
	{
		for(int j=1; j<=3; j++)
		{
			if(j==i)
				continue;
			for(int k=1; k<=3; k++)
			{
				if (k==i || k== j || j > k)
					continue;
				
				for(int s0=1; s0>=-1; s0-=2)
				for(int s1=1; s1>=-1; s1-=2)
				for(int s2=1; s2>=-1; s2-=2)
					symetries[id++]=Vector3(s0*i, s1*j, s2*k);
			}
		}
	}
	
	
	printf("Loaded %d symetries\n", id);
	for(int i=0; i<id;i++)
	{
		symetries[i].print();
	}
}

Matrix33 fromSymToMatrix(Vector3 sym)
{
	float m[3][3] = {0};
	
	for(int i=0; i<3; i++)
	{
		m[abs(sym[i])-1][i] = 1;
		if (sym[i] < 0)
			m[abs(sym[i])-1][i] = -1;
	}
	
	return Matrix33(m);
}

void compute_spheres_48(float r)
{
	Vector3 orig = Vector3();
	
	float k = floor(log2((2.0*r/sqrt(3))));
	float size = pow(2, k);
	
	printf("%f -- %f\n", k, size);
	
	colors[nb_cubes] = Vector3(0, 0, 0);
	positions[nb_cubes] = Vector3(0, 0, 0);
	sizes[nb_cubes] = size;
	nb_cubes++;
	
	Vector3 center = orig + Vector3(size/2.0, 0, 0);
	
	while(k>=0)
	{
		size/=2;
		
		Vector3 right_corner = center+Vector3(size);
		
		Vector3 current_voxel = right_corner;
		bool stay = true;
		while(1)
		{
			if ((current_voxel - orig).length() > r)
				break;
			
			while(1)
			{
				if ((current_voxel - orig).length() > r)
					break;
				
				for(int i=0; i<48; i++)
				{
					stay = false;
					colors[nb_cubes] = Vector3(1, 0, 0);
					positions[nb_cubes] = fromSymToMatrix(symetries[i])*(current_voxel-Vector3(size/2.0));;
					sizes[nb_cubes] = size;
					nb_cubes++;
				}
				current_voxel+= Vector3(0, size, 0);
			}
			
			current_voxel.y( current_voxel.z() );
			current_voxel += Vector3(0, 0, size);
		}
		
		if(!stay)
			center += Vector3(size, 0, 0);
		
		k--;
	}
	
	printf("Computed %d cubes\n", nb_cubes);
}

void compute_spheres_24(float r)
{
	Vector3 orig = Vector3();
	
	float k = floor(log2((2.0*r/sqrt(3))));
	float size = pow(2, k);
	
	printf("%f -- %f\n", k, size);
	
	colors[nb_cubes] = Vector3(0, 0, 0);
	positions[nb_cubes] = Vector3(0, 0, 0);
	sizes[nb_cubes] = size;
	nb_cubes++;
	
	Vector3 center = orig + Vector3(size/2.0, 0, 0);
	
	while(k>=0)
	{
		size/=2;
		
		Vector3 right_corner = center+Vector3(size);
		
		Vector3 current_voxel = right_corner;
		bool stay = true;
		while(1)
		{
			if ((current_voxel - orig).length() > r)
				break;
			
			while(1)
			{
				if ((current_voxel - orig).length() > r)
					break;
				
				/*for(int i=0; i<8; i++)
				{
					stay = false;
					colors[nb_cubes] = Vector3(1, 0, 0);
					positions[nb_cubes] = fromSymToMatrix(symetries[i])*(current_voxel-Vector3(size/2.0));;
					sizes[nb_cubes] = size;
					nb_cubes++;
				}*/
				for(int i=0; i<24; i++)
				{
					stay = false;
					colors[nb_cubes] = Vector3(1, 0, 0);
					positions[nb_cubes] = fromSymToMatrix(symetries[i])*(current_voxel-Vector3(size/2.0));;
					sizes[nb_cubes] = size;
					nb_cubes++;
				}
				current_voxel+= Vector3(0, size, 0);
			}
			
			current_voxel.y( right_corner.y() );
			current_voxel += Vector3(0, 0, size);
		}
		
		if(!stay)
			center += Vector3(size, 0, 0);
		
		k--;
	}
	
	printf("Computed %d cubes\n", nb_cubes);
}

float length(vec3 toto)
{
	return toto.length();
}

int main(int argc, char** argv)
{
	fillSymetries24();
	
	if (argc < 2)
	{
		printf("Usage: %s <radius>\n", argv[0]);
		return 0;
	}
	
	float radius = atof(argv[1]);
	
	if (radius <= 0)
	{
		printf("radius need to be > 0 (here it is worth %f)\n", radius);
		return 0;
	}

	GLWindow* w = new GLWindow(false, false);

	Mesh m_cube("../data/models/obj/centered_unit_cube.obj");
	Mesh m_sphere("../data/models/obj/centered_r1_sphere.obj");
	
	m_cube.box().pMin.print();
	m_cube.box().pMax.print();
	m_sphere.box().pMin.print();
	m_sphere.box().pMax.print();

	Camera c;
	c.setTo(Vector3(0, 0, -m_sphere.box().getSizes().z()*radius*1.5));

	Program draw_size_pos;
	draw_size_pos.readSource(SHADER_PATH("draw_size_pos.glsl"));
	draw_size_pos.compile();
	draw_size_pos.link();

	Matrix44 proj = Perspective();
	
	//compute_spheres_24(radius);

	glEnable(GL_DEPTH_TEST);
	
	vec3 geometry_position = vec3(0, 0, 0);

	do
	{
		glClearColor(1, 1, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		draw_size_pos.setUniform("model", m_cube.getMatrix());
		draw_size_pos.setUniform("view", c.getMatrix());
		draw_size_pos.setUniform("projection", proj);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		
		draw_size_pos.use();

		glLineWidth(1.0);
		//drawSphere
		draw_mesh(&m_sphere, &draw_size_pos, Vector3(0, 0, 1), radius);
		
		float r = radius;
		float u_size_tex = 1.0;
		
		//****** FROM GLSL*******//
		vec3 orig = geometry_position*u_size_tex;
	
		float k = log2((2.0*r/sqrt(3)));
		float size = pow(2.0, k);
		
		float total_volume = 0.0;
		
		//volume += textureLod(densities, orig/u_size_tex, k).r * pow(size, 3);
		total_volume += pow(size, 3);
		draw_mesh(&m_cube, &draw_size_pos, Vector3(1, 0, 0), size, orig);
		
		vec3 center = orig + vec3(size/2.0, 0, 0);
		while(k>=0)
		{
			size/=2.0;
			
			vec3 right_corner = center+vec3(size, 0, 0);
			
			vec3 current_voxel = right_corner;
			bool stay = true;
			while(true)
			{
				if (length(current_voxel - orig) > r)
					break;
				
				while(true)
				{
					if (length(current_voxel - orig) > r)
						break;
					
					for(int i=0; i<24; i++)
					{
						stay = false;
						vec3 pos = orig + fromSymToMatrix(symetries[i])*(current_voxel-vec3(size/2.0, 0, 0));
						draw_mesh(&m_cube, &draw_size_pos, Vector3(1, 0, 0), size, pos);
						//volume += textureLod(densities, pos/u_size_tex, k).r * pow(size, 3);
						total_volume += pow(size, 3);
					}
					current_voxel+= vec3(0, size, 0);
				}
				current_voxel.y ( right_corner.y() );
				current_voxel += vec3(0, 0, size);
			}
			
			if(!stay)
				center += vec3(size, 0, 0);
			
			k--;
		}
		//****** FROM GLSL*******//
		
		/*glLineWidth(2.0);
		for(int i=0; i<nb_cubes; i++)
		{
			draw_mesh(&m_cube, &draw_size_pos, colors[i], sizes[i], positions[i]);
		}*/
		
		draw_size_pos.unuse();	
		
		// Events handling
		w->swapAndPoll();
		
		/**Camera**/
		float speed = m_sphere.box().getSizes().z() * 0.1;
		c.moveWithKeyboard(w, speed);

		/**Object**/
		m_cube.rotateOnMouse(w);
		m_sphere.rotateOnMouse(w);
			
		if (w->isKeyPressed(GLFW_KEY_R))
		{
			draw_size_pos.reload();
		}
	
		if (w->isKeyPressedOnce(GLFW_KEY_C))
			capture("", 1024, 768);
    }
	while(!(w->isKeyPressed(GLFW_KEY_ESCAPE)) && !(w->isClosed()));

	return 0;
}

