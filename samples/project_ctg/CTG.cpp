#include "CTG.h"

char* CTG::generateHeightmap()
{
	printf("Not implemented yet\n");
	return NULL;
}

std::string CTG::generateShaderCode()
{
	return tree->generateShaderCode();
}

CTGBlend::CTGBlend()
{
	children = 0;
	nb_children = 0;
}

CTGBlend::~CTGBlend()
{
	for(int i=0; i<nb_children; i++)
		delete children[i];
	free(children);
}

void CTGBlend::addChild(CTGNode* n)
{
	nb_children+=1;
	children = (CTGNode**)realloc(children, sizeof(CTGNode*)*nb_children);
	children[nb_children-1] = n;
}

float CTGBlend::evaluate(float x, float y)
{
	float res = 0;
	for(int i=0; i<nb_children; i++)
		res += children[i]->evaluate(x, y);
	return res;
}

std::string CTGBlend::generateShaderCode()
{
	std::string s;
	
	for(int i=0; i<nb_children-1; i++)
		s += children[i]->generateShaderCode() + " + ";
	s+=children[nb_children-1]->generateShaderCode();
	
	return s;
}

float CTGCone::evaluate(float x, float y)
{
	Vector2 p(x, y);
	if ((p - center).length() < radius)
		return height * (radius - (p - center).length());
	else return 0;
}

std::string ftoa(float f)
{
	std::string s(10, ' ');
	sprintf(&s[0], "%10f", f);
	return s;
}

std::string CTGCone::generateShaderCode()
{
	std::string s("");
	s = s + "(\n"
	+ "(" 
		+ ftoa(radius) + " - clamp(length(p -" + 
		+ "vec3(" 
			+ ftoa(center.x()) + ", 0.0," + ftoa(center.y()) + ")" 
		+ ")"
	+ ", 0.0, " + ftoa(radius) + ")"
	+ ")"
	+ "/" + ftoa(radius) + " * " + ftoa(height) + ")";
	return s;
}

float CTGSinus::evaluate(float x, float y)
{
	return amplitude*sin(fqcy*x);
}

std::string CTGSinus::generateShaderCode()
{
	std::string s("");
	s = s + "(\n"
	+ ftoa(amplitude) + "*sin(" + ftoa(fqcy) + "* p.x)"
	+ ")";
	return s;
}






