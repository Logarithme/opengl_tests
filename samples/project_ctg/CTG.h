#ifndef CTG_H_
#define CTG_H_

#include <string>
#include <cstdio>
#include "../../src/mesh.h"

class CTGNode
{
public:
	virtual ~CTGNode() {}
	virtual float evaluate(float x, float y) = 0;
	virtual std::string generateShaderCode() = 0;
};

class CTGBlend : public CTGNode
{
private:
	int nb_children;
	CTGNode** children;
	
public:
	CTGBlend();
	~CTGBlend();
	
	void addChild(CTGNode*);
	float evaluate(float x, float y);
	std::string generateShaderCode();
};

class CTGCone : public CTGNode
{
private:
	float height;
	float radius;
	Vector2 center;
	
public:
	CTGCone(float h, float r, Vector2 c) { height = h; radius = r; center = c; }
	~CTGCone() {}
	
	float evaluate(float x, float y);
	std::string generateShaderCode();
};

class CTGSinus : public CTGNode
{
private:
	float amplitude;
	float fqcy;
	
public:
	CTGSinus(float a, float f) { amplitude = a; fqcy = f; }
	~CTGSinus() {}
	
	float evaluate(float x, float y);
	std::string generateShaderCode();
};

class CTG
{
private:
	CTGNode* tree;
	int size_x;
	int size_y;
	
public:
	char* generateHeightmap();
	std::string generateShaderCode();
	void setTree(CTGNode* t) { tree = t; }
};

#endif