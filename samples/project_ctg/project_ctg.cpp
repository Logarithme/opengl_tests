#include "../../src/window.h"
#include "../../src/mesh.h"
#include "../../src/program.h"
#include "../../src/camera.h"
#include "../../src/misc.h"
#include "CTG.h"

CTG* createCTG()
{
	CTGBlend* b = new CTGBlend();
	
	CTGCone* c1 = new CTGCone(10, 10, Vector2(30, 50));
	CTGCone* c2 = new CTGCone(20, 20, Vector2(50, 30));
	CTGSinus* s = new CTGSinus(1, 1);
	
	b->addChild(c1);
	b->addChild(c2);
	b->addChild(s);
	
	
	CTG* root = new CTG();
	root->setTree(b);
	
	return root;
}

int main(int argc, char** argv)
{
	GLWindow w(true, false, 4, 4);
	
	CTG* t = createCTG();
	
	Camera c;
	c.setTo(-50, -10, -100);
	
	Mesh m;
	m.loadTestGrid(1024, 1024, 100, 100);
	//m.loadMesh0FromFile(argv[1]);
	
	std::string version("#version 430\n");
	Program p;
	p.setSource(
		(version
		+ "uniform mat4 model;\n"
		+ "uniform mat4 view;\n"
		+ "uniform mat4 projection;\n"
		+ "\n"
		+ "layout (location=0) in vec3 position;\n"
		+ "layout (location=2) in vec2 texcoord;\n"
		+ "\n"
		+ "out vec3 vertex_position;\n"
		+ "void main()\n"
		+ "{\n"
		+ "	vec3 p = position;\n"
		+ "	p.y = " + t->generateShaderCode() + ";\n"
		+ " vertex_position = p;\n"
		+ "	gl_Position = projection * view * model * vec4(p, 1);\n"
		+ "}\n").c_str()
		,
		(version
		+ "out vec4 color;\n"
		+ "in vec3 vertex_position;\n"
		+ "\n"
		+ "void main()\n"
		+ "{\n"
		+ "vec3 normal = normalize(cross( dFdx(vertex_position.xyz), dFdy(vertex_position.xyz)));\n"
		+ "color = clamp(dot(normal, vec3(1)), 0, 1) * vec4(1, 0, 0, 1);\n"
		+ "}\n").c_str()
	);
	p.compile();
	p.link();
	
	do{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1, 1, 1, 1);
		
		static bool wireframe = false;
		if (wireframe)
			glPolygonMode (GL_FRONT_AND_BACK, GL_LINE); 
		else 
			glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		
		p.setUniform("projection", w.getMatrix());
		p.setUniform("view", c.getMatrix());
		p.setUniform("model", m.getMatrix());
		
		p.use();
		
		m.bindIt();
		m.drawMesh(GL_TRIANGLES);
		m.unbindIt();
		
		p.unuse();
		
		w.swapAndPoll();
		m.rotateOnMouse(&w);
		c.moveWithKeyboard(&w, 0.1);
		
		if (w.isKeyPressedOnce(GLFW_KEY_W))
			wireframe = !wireframe;
		if (w.isKeyPressedOnce(GLFW_KEY_C))
			capture("");
		
	}while( !(w.isClosed()) && !(w.isKeyPressed(GLFW_KEY_ESCAPE)));

	delete t;
	
	return 0;
}