#version 430

$start VERTEX_SHADER

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

layout (location=0) in vec3 position;
layout (location=2) in vec2 k1k2;

out vec2 vertex_k1k2;

void main()
{
	vertex_k1k2 = k1k2;
    gl_Position = projection * view * model * vec4(position, 1);
}

$end VERTEX_SHADER

$start FRAGMENT_SHADER

in vec2 vertex_k1k2;

out vec4 color;

vec3 HSVtoRGB(vec3 hsv)
{
  int i;
  double f, p, q, t;
  if( hsv.y == 0 ) {                     // achromatic (gray)
    return vec3(hsv.z);
  }
  i = int( floor( hsv.x / 60 ) );
  f = ( hsv.x / 60 ) - i;                        // factorial part of h
  p = hsv.z * ( 1.0 - hsv.y );
  q = hsv.z * ( 1.0 - hsv.y * f );
  t = hsv.z * ( 1.0 - hsv.y * ( 1.0 - f ) );

  if (i==0)
  	return vec3(hsv.z, t, p);
  if (i==1)
  	return vec3(q, hsv.z, p);
  if (i==2)
  	return vec3(p, hsv.z, t);
  if (i==3)
  	return vec3(p, q, hsv.z);
  if (i==4)
  	return vec3(t, p, hsv.z);

  return vec3(hsv.z, p, q);
}

vec3 colormap(float scale)
{
  float cycles = 1;
  const double hue = 360 * ( scale * cycles - floor(scale * cycles));
  return HSVtoRGB( vec3(hue, 0.9, 1.0) );
}

vec3 colorFromCurv(float c)
{
	vec3 color;
	float gt_curvature = c+0.5;
	color = colormap(gt_curvature);
	
	return color;
}

void main()
{	
	float k1 = vertex_k1k2.x;
	float k2 = vertex_k1k2.y;
    color = vec4( colorFromCurv((k1+k2/2.0)), 1);
}
$end FRAGMENT_SHADER