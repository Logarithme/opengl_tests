/**
* Load and display a mesh
**/

#include "../../src/window.h"
#include "../../src/mesh.h"
#include "../../src/program.h"
#include "../../src/geo_math.h"
#include "../../src/texture.h"
#include "../../src/misc.h"
#include "../../src/camera.h"
#include "../../src/framebuffer.h"
#include "../../src/scene.h"

#include <cstdio>
#include <cstdlib>

#define SHADER_PATH(x) "../samples/read_exported_curv/" x

void readExportFile(char* file, Mesh* m)
{
	FILE* plotfd = fopen(file,"r");
	if (plotfd == NULL)
		perror("fopen");
	
	Vector3* geom;
	Vector2* k1k2;
	
	int nb = 0;
	int taille = 0;
	while ( !feof(plotfd) )
	{
		char line[256];
		fgets(line, 256, plotfd);
		
		if ((line[0] == '#') || (line[0] == '\n'))
			continue;
		
		if (line[0] == ('N'))
		{
            sscanf(line, "N %d\n", &taille);
			geom = new Vector3[taille];
			k1k2 = new Vector2[taille];
			continue;
		}
		
		double x, y, z;
		double k1, k2;
		sscanf(line, "%lf\t %lf\t %lf\t \
						%lf\t %lf\t \
						%*lf\t %*lf\t %*lf\t \
						%*lf\t %*lf\t %*lf\n", &x, &y, &z, &k1, &k2);
		
		k1k2[nb] = Vector2(k1, k2);
		geom[nb++] = Vector3(x, y, z);
	}
	
	fclose(plotfd);
	
	m->loadGeometry(geom, nb);
	m->addBuffer(nb, 2, sizeof(float), GL_FLOAT, k1k2);
	
	delete [] geom;
	delete [] k1k2;
}

int main(int argc, char** argv)
{
	if (argc <= 1)
	{
		printf("\n\n Usage %s <mesh_file> \n\n", argv[0]);
		return 0;
	}

	GLWindow* w = new GLWindow(false, true);

	Mesh m;
	readExportFile(argv[1], &m);

	Program shader;
	shader.readSource(SHADER_PATH("draw.glsl"));
	shader.compile();
	shader.link();

	Camera* c = new Camera();
	Vector3 cpos = Vector3(0, 0, -2.5*m.box().getSizes().z()) + m.box().getMidPoint();
	c->setTo(Vector3(cpos.x(), cpos.y()*-1, cpos.z()));
	
	Matrix44 proj = Perspective();
	
	glEnable(GL_DEPTH_TEST);

	do
	{
		glClearColor(1, 1, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glPointSize(10.0);
		
		shader.setUniform("model", m.getMatrix());
		shader.setUniform("view", c->getMatrix());
		shader.setUniform("projection", proj);
		
		shader.use();
		m.bindIt();
		m.drawMesh(GL_POINTS);
		m.unbindIt();
		shader.unuse();
		
		// Events handling
		w->swapAndPoll();
		
		/**Camera**/
		float speed = m.box().getSizes().z() * 0.01;
		c->moveWithKeyboard(w, speed);

		/**Object**/
		m.rotateOnMouse(w);
			
		if (w->isKeyPressed(GLFW_KEY_R))
		{
			shader.reload();
		}
	
		if (w->isKeyPressedOnce(GLFW_KEY_C))
			capture("", 1024, 768);
    }
	while(!(w->isKeyPressed(GLFW_KEY_ESCAPE)) && !(w->isClosed()));

	return 0;
}
