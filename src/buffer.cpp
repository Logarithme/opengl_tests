#include "buffer.h"

ShaderStorageBufferObject::ShaderStorageBufferObject()
{
	glGenBuffers(1, &id);
	printf("Creating SSBO %d\n", id);
}

ShaderStorageBufferObject::~ShaderStorageBufferObject()
{
	printf("Deleting SSBO %d\n", id);
	glDeleteBuffers(1, &id);
}

void ShaderStorageBufferObject::setData(void* data, int nb_elements, int element_size, GLenum usage)
{
	buffer_size = nb_elements;
	elem_size = element_size;
	
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, id);
	glBufferData(GL_SHADER_STORAGE_BUFFER, nb_elements * element_size, data, usage);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void* ShaderStorageBufferObject::mapToCPU(char access, int begin, int end)
{
	if (begin == -1)
		begin = 0;
	if (end == -1)
		end = buffer_size;
	
	GLint bufMask = 0;
	if (access == 'w' || access == 'x')
		bufMask |= GL_MAP_WRITE_BIT;
	if (access == 'r' || access == 'x')
		bufMask |= GL_MAP_READ_BIT;
	
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, id);
	return glMapBufferRange(GL_SHADER_STORAGE_BUFFER, begin*elem_size, end*elem_size, bufMask);
}

void ShaderStorageBufferObject::unmap()
{
	glUnmapBuffer( GL_SHADER_STORAGE_BUFFER );
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void ShaderStorageBufferObject::bind(int binding_point)
{
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding_point, id);
}




