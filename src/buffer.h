#ifndef BUFFER_H_
#define BUFFER_H_

#include <cstdio>
#include <GL/glew.h>
#include <GL/gl.h>

class ShaderStorageBufferObject
{
private:
	GLuint id;
	int buffer_size;
	int elem_size;
	
public:
	ShaderStorageBufferObject();
	~ShaderStorageBufferObject();
	
	/**
	 * usage form : GL_frequency_nature
	 * frequency
	 * 		STREAM
     *          The data store contents will be modified once and used at most a few times.
     *      STATIC
     *          The data store contents will be modified once and used many times.
     *      DYNAMIC
     *          The data store contents will be modified repeatedly and used many times.
     * 
	 * nature  
	 *		DRAW
     *          The data store contents are modified by the application, and used as the source for GL drawing and
     *          image specification commands.
     *      READ
     *          The data store contents are modified by reading data from the GL, and used to return that data 
     *          when queried by the application.
     *      COPY
     *          The data store contents are modified by reading data from the GL, and used as the source for GL
     *          drawing and image specification commands.
     */                               
	void setData(void* data, int nb_elements, int element_size, GLenum usage);
	/*
	 * access :
	 * 'r' - read only
	 * 'w' - write only
	 * 'x' - read & write
	 */
	void* mapToCPU(char access, int begin=-1, int end=-1);
	void unmap();
	
	void bind(int binding_point);
};

typedef ShaderStorageBufferObject SSBO;

#endif