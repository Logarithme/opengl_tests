#include "camera.h"

#include<cstdio>

void Camera::moveWithKeyboard(GLWindow* w, float speed)
{
    if(w->isKeyPressed(GLFW_KEY_UP))
    {
        if (  w->isKeyPressed(GLFW_KEY_LEFT_CONTROL)  )
	{
	    view = view * Translate(Vector3(0, 0, speed));
	    position += Vector3(0, 0, speed);
	}
        else
	{
            view = view * Translate(Vector3(0, -speed, 0));
	    position += Vector3(0, -speed, 0);
	}
    }
    
    if(w->isKeyPressed(GLFW_KEY_DOWN))
    {
        if (  w->isKeyPressed(GLFW_KEY_LEFT_CONTROL)  )
	{
            view = view * Translate(Vector3(0, 0, -speed));
	    position += Vector3(0, 0, -speed);
	}
        else
	{
            view = view * Translate(Vector3(0, speed, 0));
	    position += Vector3(0, speed, 0);
	}
    }
    
    if(w->isKeyPressed(GLFW_KEY_RIGHT))
    {
        view = view * Translate(Vector3(-speed, 0, 0));
	position += Vector3(-speed, 0, 0);
    }
    if(w->isKeyPressed(GLFW_KEY_LEFT))
    {
        view = view * Translate(Vector3(speed, 0, 0));
	position += Vector3(speed, 0, 0);
    }
    
    if(w->isKeyPressed(GLFW_KEY_PAGE_UP))
    {
        view = view * Translate(Vector3(0, 0, speed*10));
	position += Vector3(0, 0, speed*10);
    }
    if(w->isKeyPressed(GLFW_KEY_PAGE_DOWN))
    {
        view = view * Translate(Vector3(0, 0, speed*-10));
	position += Vector3(0, 0, speed*-10);
    }
}

void Camera::moveWithKeyboardAndMouse(GLWindow* w, float speed)
{
	moveWithKeyboard(w, speed);
	
	if (w->isClikedLeft())
        {
            double x, y;
            w->getMouseMove(&x, &y);
	    
	    view = RotateX(-y*0.001) * view;
	    view = RotateY(-x*0.001) * view;
        }
}


void Camera::lookAt(Vector3 center, Vector3 up)
{
	printf("Camera::LookAt UNTESTED\n");
	Vector3 eye = getPosition();
	
	Vector3 f = center - eye;
	f = f.normalize();
	
	up = up.normalize();
	
	Vector3 s = CrossProduct(f, up);
	Vector3 u = CrossProduct(s.normalize(), f);
	
	Matrix44 M = Matrix44(
	                s.x(), s.y(), s.z(), 0,
	                u.x(), u.y(), u.z(), 0,
	                -f.x(), -f.y(), -f.z(), 0,
	                0, 0, 0, 1
	                );
	
	view =  M * Translate(eye * -1);
}

void Camera::eyeDirection(Vector3 dir, Vector3 up)
{
	Vector3 f = dir;
	f = f.normalize();
	
	up = up.normalize();
	
	Vector3 s = CrossProduct(f, up);
	Vector3 u = CrossProduct(s.normalize(), f);
	
	Matrix44 M = Matrix44(
	                s.x(), s.y(), s.z(), 0,
	                u.x(), u.y(), u.z(), 0,
	                -f.x(), -f.y(), -f.z(), 0,
	                0, 0, 0, 1
	                );
	
	Vector3 eye = getPosition();
	view =  M * Translate(eye * -1);
}
 

Matrix44 QuaternionCamera::getMatrix()
{
	/*Quaternion v2 = view.normalize();
	
	float x = v2.v().x();
	float y = v2.v().y();
	float z = v2.v().z();
	float w = v2.w();

	Matrix44 rot = Matrix44(
	                1 - y*y - 2*z*z , 2*x*y + 2*w*z , 2*x*z + 2*w*y , 0,
					2*x*y - 2*w*z , 1 - 2*x*x - 2*z*z , 2*y*z + 2*w*x , 0,
					2*x*z + 2*w*y , 2*y*z - 2*w*x , 1 - 2*x*x - 2*y*y ,	0,
					0,   0,   0,   1
	                );
	return rot * Translate(position);*/
    
	Vector3 aim = view.v();
	Vector3 f = aim.normalize();

	Vector3 s = CrossProduct(f, up.v().normalize());
	Vector3 u = CrossProduct(s.normalize(), f);



	Matrix44 M = Matrix44(
	                s.x(), s.y(), s.z(), 0,
	                u.x(), u.y(), u.z(), 0,
	                -f.x(), -f.y(), -f.z(), 0,
	                0, 0, 0, 1
	                );
	
	return M * Translate(position * -1);
}

void QuaternionCamera::moveWithKeyboard(GLWindow* w, float speed)
{
    if(w->isKeyPressed(GLFW_KEY_UP))
    {
        if (  w->isKeyPressed(GLFW_KEY_LEFT_CONTROL)  )
            position.z( position.z() - speed );
        else
            position.y( position.y() + speed );
    }
    if(w->isKeyPressed(GLFW_KEY_DOWN))
    {
        if (  w->isKeyPressed(GLFW_KEY_LEFT_CONTROL)  )
            position.z( position.z() + speed );
        else
            position.y( position.y() - speed );
    }
    if(w->isKeyPressed(GLFW_KEY_RIGHT))
        position.x( position.x() + speed );
    if(w->isKeyPressed(GLFW_KEY_LEFT))
        position.x( position.x() - speed );
}

void QuaternionCamera::moveWithKeyboardAndMouse(GLWindow* w, float speed)
{	
	Vector3 axis = CrossProduct(view.v().normalize(), Vector3(0, 1, 0)).normalize();
	
	if(w->isKeyPressed(GLFW_KEY_Z))
		position = position + view.v().normalize() * (speed);
	if(w->isKeyPressed(GLFW_KEY_S))
		position = position - view.v().normalize() * (speed);

	if (w->isClikedLeft())
        {
            double x, y;
            w->getMouseMove(&x, &y);
            
            view.rotateAround(x/1000.f, Vector3(0, 1, 0));
            view.rotateAround(y/1000.f, axis);
            
            up.rotateAround(x/1000.f, Vector3(0, 1, 0));
            up.rotateAround(y/1000.f, axis);
        }
}
