#ifndef CAMERA_H_
#define CAMERA_H_

#include "geo_math.h"
#include "window.h"

class Camera
{
    private:
	Vector3 position;
        Matrix44 view;
    
    public:

        Matrix44 getMatrix() { return view; }
        void setMatrix(Matrix44 v) { view = v; }
        
        Vector3 getPosition() { return position; }
        void setTo( Vector3 pos ) { view = Translate(pos); position = pos; }
        void setTo( double x, double y, double z ) { view = Translate(Vector3(x, y, z)); position = Vector3(x, y, z);}
        
        void lookAt(Vector3 center, Vector3 up = Vector3(0, 1, 0));
	void eyeDirection(Vector3 dir, Vector3 up  = Vector3(0, 1, 0));
	
        void moveWithKeyboard(GLWindow* w, float speed=1.0);
	void moveWithKeyboardAndMouse(GLWindow* w, float speed=1.0);
};

class QuaternionCamera
{
	private:
		Vector3 position;
                Quaternion up;
		Quaternion view;
		
	public:
                QuaternionCamera() { position = Vector3(0, 0, 0); up = Quaternion(0, 1, 0, 0); }
		Matrix44 getMatrix();

		Vector3 getPosition() { return position; }
		void setTo( Vector3 v ) { position = v; }
		void moveWithKeyboard(GLWindow* w, float speed=1.0);
		void moveWithKeyboardAndMouse(GLWindow* w, float speed=1.0);
};

#endif
