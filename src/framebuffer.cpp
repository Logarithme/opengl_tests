#include "framebuffer.h"

#include <cstdio>
#include <boost/concept_check.hpp>

void Framebuffer::attachColorTexture(Texture2D& t)
{
	glBindFramebuffer(GL_FRAMEBUFFER, Id);
	// attach the texture to FBO color attachment point
	glFramebufferTexture2D(GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
                       GL_COLOR_ATTACHMENT0+draw_buffers.size(),  // 2. attachment point
                       GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
                       t.getId(),             // 4. tex ID
                       0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	draw_buffers.push_back(GL_COLOR_ATTACHMENT0+draw_buffers.size());
}

void Framebuffer::attachDepthRBO()
{
	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1024, 768);
	
	glBindFramebuffer(GL_FRAMEBUFFER, Id);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	/*
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, t.getId(), 0);
	*/
}

void Framebuffer::set()
{
	glBindFramebuffer(GL_FRAMEBUFFER, Id);
	glDrawBuffers(draw_buffers.size(), &draw_buffers[0]);
}



