#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

#include "texture.h"

#include <GL/glew.h>
#include <GL/gl.h>
#include <string>
#include <vector>

class Framebuffer
{
private:
	GLuint Id;
	
	unsigned int sizex;
	unsigned int sizey;

	std::vector<GLenum> draw_buffers;
public:
	Framebuffer() { glGenFramebuffers(1, &Id); }
	
	void attachColorTexture(Texture2D& t);
	void attachDepthRBO();
	
	void set();
	void unset() { glBindFramebuffer(GL_FRAMEBUFFER, 0); }
};

#endif