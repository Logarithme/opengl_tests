#include "geo_math.h"

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <algorithm>


/** Vector2 **/

void Vector2::print() const
{
    printf("(%f, %f)\n", xy[0], xy[1]);
}

Vector2 Vector2::operator-(Vector2 v2)
{
    return Vector2(xy[0] - v2.x(), xy[1] - v2.y());
}
Vector2 Vector2::operator+(Vector2 v2)
{
    return Vector2(xy[0] + v2.x(), xy[1] + v2.y());
}

float DotProduct(const Vector2& v1, const Vector2& v2)
{
	return v1.x()*v2.x() + v1.y()*v2.y();
}

/**Vector3**/

void Vector3::print() const
{
    printf("(%f, %f, %f)\n", xyz[0], xyz[1], xyz[2]);
}

Vector3 Vector3::operator-(Vector3 v2)
{
    return Vector3(xyz[0] - v2.x(), xyz[1] - v2.y(), xyz[2] - v2.z());
}
Vector3 Vector3::operator+(Vector3 v2)
{
    return Vector3(xyz[0] + v2.x(), xyz[1] + v2.y(), xyz[2] + v2.z());
}

Vector3 Vector3::operator+=(Vector3 v2)
{
	xyz[0] += v2.x();
	xyz[1] += v2.y();
	xyz[2] += v2.z();
	
	return Vector3(xyz);
}


Vector3 CrossProduct(const Vector3& v1, const Vector3& v2)
{
	return Vector3(
	               v1.y()*v2.z() - v2.y()*v1.z(),
	               v1.z()*v2.x() - v2.z()*v1.x(),
	               v1.x()*v2.y() - v2.x()*v1.y()
	               );
}

float DotProduct(const Vector3& v1, const Vector3& v2)
{
	return v1.x()*v2.x() + v1.y()*v2.y() + v1.z()*v2.z();
}

/**Matrix33**/
#define MAT33(m, x, y) m[x*3 + y]
Matrix33::Matrix33(const float e00, const float e01, const float e02,
                            const float e10, const float e11, const float e12,
                            const float e20, const float e21, const float e22)
{
	MAT33(mat, 0, 0) = e00;
    MAT33(mat, 1, 0) = e10;
    MAT33(mat, 2, 0) = e20;
    
    MAT33(mat, 0, 1) = e01;
    MAT33(mat, 1, 1) = e11;
    MAT33(mat, 2, 1) = e21;
    
    MAT33(mat, 0, 2) = e02;
    MAT33(mat, 1, 2) = e12;
    MAT33(mat, 2, 2) = e22;
}

Matrix33::Matrix33(const float dat[9])
{
	for(int i=0; i<9; i++)
        mat[i] = dat[i];
}

Matrix33::Matrix33(const float dat[3][3])
{
	for(int i=0; i<3; i++)
	for(int j=0; j<3; j++)
        mat[i*3+j] = dat[i][j];
}

Matrix33::Matrix33()
{
	for(int i=0; i<9; i++)
        mat[i] = 0;
	mat[0] = 1;
	mat[4] = 1;
	mat[9] = 1;
}

Vector3 Matrix33::operator*(Vector3 v)
{
	Vector3 r;
	r.x(v.x()*mat[0]+v.y()*mat[1]+v.z()*mat[2]);
	r.y(v.x()*mat[3]+v.y()*mat[4]+v.z()*mat[5]);
	r.z(v.x()*mat[6]+v.y()*mat[7]+v.z()*mat[8]);
	return r;
}

void Matrix33::print() const
{
	printf("[%f, \t %f, \t %f]\n", MAT33(mat, 0, 0), MAT33(mat, 0, 1), MAT33(mat, 0, 2));
	printf("[%f, \t %f, \t %f]\n", MAT33(mat, 1, 0), MAT33(mat, 1, 1), MAT33(mat, 1, 2));
	printf("[%f, \t %f, \t %f]\n", MAT33(mat, 2, 0), MAT33(mat, 2, 1), MAT33(mat, 2, 2));
}

Matrix33 Matrix33::transpose()
{
	float r[9] = {0};
    
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            MAT33(r, i, j) = MAT33(mat, j, i);
    
    return Matrix33(r);
}

/**Matrix44**/

#define MAT44(m, x, y) m[x*4 + y]
Matrix44::Matrix44() 
{
    for(int i=0; i<16; i++)
        mat[i] = 0;
    for(int i=0; i<4; i++)
        MAT44(mat, i, i) = 1;
}

Matrix44::Matrix44(const float dat[4][4])
{
    for(int row=0; row<4; row++)
    for(int col=0; col<4; col++)
    {
        MAT44(mat, row, col) = dat[row][col];
    }
}
Matrix44::Matrix44(const float e00, const float e01, const float e02, const float e03,
                            const float e10, const float e11, const float e12, const float e13,
                            const float e20, const float e21, const float e22, const float e23,
                            const float e30, const float e31, const float e32, const float e33)
{
    MAT44(mat, 0, 0) = e00;
    MAT44(mat, 1, 0) = e10;
    MAT44(mat, 2, 0) = e20;
    MAT44(mat, 3, 0) = e30;
    
    MAT44(mat, 0, 1) = e01;
    MAT44(mat, 1, 1) = e11;
    MAT44(mat, 2, 1) = e21;
    MAT44(mat, 3, 1) = e31;
    
    MAT44(mat, 0, 2) = e02;
    MAT44(mat, 1, 2) = e12;
    MAT44(mat, 2, 2) = e22;
    MAT44(mat, 3, 2) = e32;
    
    MAT44(mat, 0, 3) = e03;
    MAT44(mat, 1, 3) = e13;
    MAT44(mat, 2, 3) = e23;
    MAT44(mat, 3, 3) = e33;
}
Matrix44::Matrix44(const float dat[16])
{
     for(int i=0; i<16; i++)
        mat[i] = dat[i];
}

void Matrix44::print() const
{
    printf("[%f, \t %f, \t %f, \t %f]\n", MAT44(mat, 0, 0), MAT44(mat, 0, 1), MAT44(mat, 0, 2), MAT44(mat, 0, 3));
    printf("[%f, \t %f, \t %f, \t %f]\n", MAT44(mat, 1, 0), MAT44(mat, 1, 1), MAT44(mat, 1, 2), MAT44(mat, 1, 3));
    printf("[%f, \t %f, \t %f, \t %f]\n", MAT44(mat, 2, 0), MAT44(mat, 2, 1), MAT44(mat, 2, 2), MAT44(mat, 2, 3));
    printf("[%f, \t %f, \t %f, \t %f]\n", MAT44(mat, 3, 0), MAT44(mat, 3, 1), MAT44(mat, 3, 2), MAT44(mat, 3, 3));
}

Matrix44 Matrix44::operator*(Matrix44 m)
{
    float* mat2 = m.data();
    float r[16] = {0};
    
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            for(int k=0; k<4; k++)
                MAT44(r, i, j) += MAT44(mat, i, k) * MAT44(mat2, k, j);
    
        return Matrix44(r);
}

Matrix44 Matrix44::transpose()
{
    float r[16] = {0};
    
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            MAT44(r, i, j) = MAT44(mat, j, i);
    
    return Matrix44(r);
}

#undef MAT44
                        
Matrix44 Translate(const Vector3& axis)
{
    return Matrix44(    
                                1, 0, 0, axis.x(),
                                0, 1, 0, axis.y(),
                                0, 0, 1, axis.z(),
                                0, 0, 0, 1
                            );
}

Matrix44 Scale(const Vector3& axis)
{
    return Matrix44(    
                                axis.x(), 0, 0, 0,
                                0, axis.y(), 0, 0,
                                0, 0, axis.z(), 0,
                                0, 0, 0, 1
                            );
}

Matrix44 RotateX(const float angle)
{
    return Matrix44(
        1, 0, 0, 0,
        0, cos(angle), -1*sin(angle) , 0,
        0, sin(angle), cos(angle), 0,
        0, 0, 0, 1
    );
}

Matrix44 RotateY(const float angle)
{
    return Matrix44(
        cos(angle), 0, sin(angle), 0,
        0, 1, 0, 0,
        -sin(angle), 0, cos(angle), 0,
        0, 0, 0, 1
    );
}

Matrix44 RotateZ(const float angle)
{
    return Matrix44(
        cos(angle), -sin(angle), 0, 0,
        sin(angle), cos(angle), 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    );
}

//gKit functions
Matrix44 Perspective(const float fov, const float ratio, const float near, const float far)
{
    const float inv_tan = 1.f / tanf( RAD( fov / 2.f ) );
    const float inv_denom = 1.f / ( near - far );
    return Matrix44(    
                                inv_tan/ratio, 0,       0,                    0,
                                0,             inv_tan, 0,                    0,
                                0,             0,       (near+far)*inv_denom, 2.f*far*near*inv_denom,
                                0,             0,       -1,                   0
                );
}

Matrix44 Orthographic( const float left, const float right, 
    const float bottom, const float top, 
    const float znear, const float zfar )
{
    // opengl version
    Matrix44 ortho(
        2.f / (right - left)     , 0.f                 , 0.f                  , -(right + left) / (right - left),
        0.f                      , 2.f / (top - bottom), 0.f                  , -(top + bottom) / (top - bottom),
        0.f                      , 0.f                 , -2.f / (zfar - znear), -(zfar + znear) / (zfar - znear),
        0.f, 0.f, 0.f, 1.f
    );
    
    return ortho;
}

Matrix44 Viewport(float width, float height)
{
    const float w= width / 2.f;
    const float h= height / 2.f;
    
    return Matrix44(
        w, 0,   0,   w,
        0, h,   0,   h,
        0, 0, .5f, .5f,
        0, 0,   0,   1
    );
}

Matrix44 Matrix44::inverse()
{
    int indxc[4], indxr[4];
    int ipiv[4] = { 0, 0, 0, 0 };
    
    float minv[4][4];
    memcpy(minv, mat, sizeof(float[16]));
    
    for (int i = 0; i < 4; i++) {
        int irow = -1, icol = -1;
        float big = 0.f;
        
        // Choose pivot
        for (int j = 0; j < 4; j++) {
            if (ipiv[j] != 1) {
                for (int k = 0; k < 4; k++) {
                    if (ipiv[k] == 0) {
                        if (fabsf(minv[j][k]) >= big) {
                            big = float(fabsf(minv[j][k]));
                            irow = j;
                            icol = k;
                        }
                    }
                    else if (ipiv[k] > 1)
                        printf("Singular matrix in Matrix4x4::getInverse()\n");
                }
            }
        }
        
        ++ipiv[icol];
        // Swap rows _irow_ and _icol_ for pivot
        if (irow != icol) {
            for (int k = 0; k < 4; ++k)
                std::swap(minv[irow][k], minv[icol][k]);
        }
        
        indxr[i] = irow;
        indxc[i] = icol;
        if (minv[icol][icol] == 0.)
            printf("Singular matrix in Matrix4x4::getInverse()\n");
        
        // Set $m[icol][icol]$ to one by scaling row _icol_ appropriately
        float pivinv = 1.f / minv[icol][icol];
        minv[icol][icol] = 1.f;
        for (int j = 0; j < 4; j++)
            minv[icol][j] *= pivinv;
        
        // Subtract this row from others to zero out their columns
        for (int j = 0; j < 4; j++) {
            if (j != icol) {
                float save = minv[j][icol];
                minv[j][icol] = 0;
                for (int k = 0; k < 4; k++)
                    minv[j][k] -= minv[icol][k]*save;
            }
        }
    }
    
    // Swap columns to reflect permutation
    for (int j = 3; j >= 0; j--) {
        if (indxr[j] != indxc[j]) {
            for (int k = 0; k < 4; k++)
                std::swap(minv[k][indxr[j]], minv[k][indxc[j]]);
        }
    }
    
    return Matrix44(minv);
}

/**Quaternion**/

float Quaternion::length() const
{
	return sqrt( _w*_w + _v.x()*_v.x() + _v.y()*_v.y() + _v.z()*_v.z());
}

Quaternion Quaternion::normalize() const
{
	return Quaternion( _v / length(), _w / length() );
}

Quaternion Quaternion::conjugate() const
{
	return Quaternion( _v * -1, _w );
}

Quaternion Quaternion::operator*(const Quaternion& q) const
{
	float ax = _v.x();
	float ay = _v.y();
	float az = _v.z();
	float aw = _w;

	float bx = q.v().x();
	float by = q.v().y();
	float bz = q.v().z();
	float bw = q.w();


	float w2 = aw*bw - ax*bx - ay*by - az*bz;
	Vector3 v2(
	           aw*bx + ax*bw + ay*bz - az*by,
	           aw*by - ax*bz + ay*bw + az*bx,
	           aw*bz + ax*by - ay*bx + az*bw
	           );

	return Quaternion(v2, w2);
}

void Quaternion::rotateAround(float angle, Vector3 axis)
{
	Quaternion temp(axis.x() * sin(angle/2), axis.y() * sin(angle/2), axis.z() * sin(angle/2), cos(angle/2));
	Quaternion quat_view (_v, 0);

	Quaternion result = temp.conjugate() * quat_view * temp;
	result = result.normalize();

	_v.x(result.v().x());
	_v.y(result.v().y());
	_v.z(result.v().z());
}


