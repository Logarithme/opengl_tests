#ifndef GEO_MATH_H_
#define GEO_MATH_H_

#define PI 3.14159f
#define RAD(x) PI*x / 180.f

#include <cmath>

class Vector2
{
    private:
        float xy[2];
    
    public:
        Vector2(const float x, const float y) { xy[0] = x; xy[1] = y; }
        Vector2(const float xy_[2]) { for(int i=0; i<2; i++) xy[i] = xy_[i]; }
        Vector2(const double xy_[2]) { for(int i=0; i<2; i++) xy[i] = xy_[i]; }
        Vector2() { for(int i=0; i<2; i++) xy[i] = 0; }
    
        float x() const { return xy[0]; }
        float y() const { return xy[1]; }
        void x(float v) { xy[0] = v; }
        void y(float v) { xy[1] = v; }
        
        void print() const;

	Vector2 operator+(Vector2 v);
	Vector2 operator+=(Vector2 v) { xy[0]+= v.x(); xy[1] += v.y(); return *this; }
	Vector2 operator-(Vector2 v);
	Vector2 operator-=(Vector2 v) { xy[0]-= v.x(); xy[1] -= v.y(); return *this; }

	Vector2 operator/(float f) const { return Vector2( xy[0] / f, xy[1] / f); }
        Vector2 operator*(float f) const { return Vector2( xy[0] * f, xy[1] * f); }
        
        float length() const { return sqrt(x()*x() + y()*y()); }
        Vector2 normalize() const { Vector2 tmp(xy); return tmp/length(); }
};

float DotProduct(const Vector2& v1, const Vector2& v2);

class Vector3
{
    private:
        float xyz[3];
    
    public:
        Vector3(const float x, const float y, const float z) { xyz[0] = x; xyz[1] = y; xyz[2] = z; }
        Vector3(const float xyz_[3]) { for(int i=0; i<3; i++) xyz[i] = xyz_[i]; }
        Vector3(const double xyz_[3]) { for(int i=0; i<3; i++) xyz[i] = xyz_[i]; }
        Vector3() { for(int i=0; i<3; i++) xyz[i] = 0; }
        Vector3(const float x) { for(int i=0; i<3; i++) xyz[i] = x; }
    
        float x() const { return xyz[0]; }
        float y() const { return xyz[1]; }
        float z() const { return xyz[2]; }
        void x(float v) { xyz[0] = v; }
        void y(float v) { xyz[1] = v; }
        void z(float v) { xyz[2] = v; }
        
        void print() const;

	Vector3 operator+(Vector3 v);
        Vector3 operator-(Vector3 v);
	
	Vector3 operator+=(Vector3 v);

	Vector3 operator/(float f) const { return Vector3(xyz[0] / f, xyz[1] / f, xyz[2] / f); }
	Vector3 operator*(float f) const { return Vector3(xyz[0] * f, xyz[1] * f, xyz[2] * f); }

	float operator[](int i) const { return xyz[i]; }
	
	float length() const { return sqrt(x()*x() + y()*y() + z()*z()); }
	Vector3 normalize() const { Vector3 tmp(xyz); return tmp/length(); }
};

Vector3 CrossProduct(const Vector3& v1, const Vector3& v2);
float DotProduct(const Vector3& v1, const Vector3& v2);

class Vector4
{
    private:
        float xyzw[4];
    
    public:
        Vector4(const float x, const float y, const float z, const float w) { xyzw[0] = x; xyzw[1] = y; xyzw[2] = z; xyzw[3] = w; }
        Vector4(const float xyzw_[4]) { for(int i=0; i<3; i++) xyzw[i] = xyzw_[i]; }
        Vector4(const unsigned char xyzw_[4]) { for(int i=0; i<3; i++) xyzw[i] = xyzw_[i]; }
        Vector4() { for(int i=0; i<4; i++) xyzw[i] = 0; }
    
        float x() const { return xyzw[0]; }
        float y() const { return xyzw[1]; }
        float z() const { return xyzw[2]; }
        float w() const { return xyzw[3]; }
        void x(float v) { xyzw[0] = v; }
        void y(float v) { xyzw[1] = v; }
        void z(float v) { xyzw[2] = v; }
        void w(float v) { xyzw[3] = v; }

};

class Matrix33
{
    private:
        float mat[9];
    
    public:
        Matrix33();
        Matrix33(const float[3][3]);
        Matrix33(const float[9]);
        Matrix33(const float, const float, const float,
                        const float, const float, const float,
                        const float, const float, const float);
    
        Matrix33 transpose();
    
        float* data() { return mat; }
        void print() const;
        
        Vector3 operator*(Vector3);
};

class Matrix44
{
    private:
        float mat[16];
    
    public:
        Matrix44();
        Matrix44(const float[4][4]);
        Matrix44(const float[16]);
        Matrix44(const float, const float, const float, const float,
                        const float, const float, const float, const float,
                        const float, const float, const float, const float,
                        const float, const float, const float, const float);
    
        Matrix44 transpose();
        Matrix44 inverse();
    
        float* data() { return mat; }
        void print() const;
        
        Matrix44 operator*(Matrix44);
};

Matrix44 Translate(const Vector3& axis);
Matrix44 Scale(const Vector3& axis);
Matrix44 RotateX(const float angle);
Matrix44 RotateY(const float angle);
Matrix44 RotateZ(const float angle);
Matrix44 Rotate(const Vector3& rotation);
Matrix44 Perspective(const float fov = 45.f, const float ratio = (1024.f/768.f) , const float near = 0.1f, const float far = 10000);
Matrix44 Orthographic( const float left, const float right, const float bottom, const float top, const float znear, const float zfar );
Matrix44 Viewport(float width=1024.f, float height=768.f);

class Quaternion
{
	private:
		Vector3 _v;
		float _w;
	public:
		Quaternion() { _w = 0; _v = Vector3(0, 0, -1); }
		Quaternion(float i, float j, float k, float l) { _v = Vector3(i, j ,k); _w = l; }
		Quaternion(Vector3 n, float l) { _v = n; _w = l; }
		Quaternion(float* t) { _v = Vector3(t[0], t[1], t[2]); _w = t[3]; }
		Quaternion(const Quaternion& q) { _v = q.v(); _w = q.w(); }

		Vector3 v() const { return _v; }
		float w() const { return _w; } 

		float length() const;
		Quaternion normalize() const;
		Quaternion conjugate() const;

		void rotateAround(float angle, Vector3 axis);

		Quaternion operator* (const Quaternion&) const;
};

#endif
