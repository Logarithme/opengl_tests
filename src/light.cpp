#include "light.h"

void Light::lookAt(Vector3 center, Vector3 up)
{
	Vector3 eye = position;
	
	Vector3 f = center - eye;
	f = f.normalize();
	
	up = up.normalize();
	
	Vector3 s = CrossProduct(f, up);
	Vector3 u = CrossProduct(s.normalize(), f);
	
	Matrix44 M = Matrix44(
	                s.x(), s.y(), s.z(), 0,
	                u.x(), u.y(), u.z(), 0,
	                -f.x(), -f.y(), -f.z(), 0,
	                0, 0, 0, 1
	                );
	
	
	view = M * Translate(eye * -1);
}