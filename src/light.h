#ifndef LIGHT_H_
#define LIGHT_H_

#include "geo_math.h"

class Light
{
    private:
	Vector3 position;
        Matrix44 view;
    
    public:

        Matrix44 getMatrix() { return view; }
        
        Vector3 getPosition() { return position; }
        void setTo( Vector3 pos ) { view = Translate(pos); position = pos; }
        void lookAt(Vector3 center, Vector3 up = Vector3(0, 1, 0));
};

#endif
