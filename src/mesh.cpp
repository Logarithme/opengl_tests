#include "mesh.h"

#include <cstdio>
#include <fstream>
#include <vector>
#include <boost/concept_check.hpp>

Mesh::Mesh() : indices_buffer(0), geometry_buffer(0)
{
    glGenVertexArrays(1, &vao_id);
    nb_indices = 0;
    nb_vertices = 0;
    nb_buffers = 0;
}

Mesh::Mesh(const char* file) : indices_buffer(0), geometry_buffer(0)
{
    glGenVertexArrays(1, &vao_id);
    nb_indices = 0;
    nb_vertices = 0;
    nb_buffers = 0;
    
    loadMesh0FromFile(file);
}

void Mesh::loadIndices(unsigned int* i_, unsigned int nb_indices_)
{
    glGenBuffers(1, &indices_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, nb_indices_*sizeof(unsigned int), i_, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    nb_indices = nb_indices_;
    
    glBindVertexArray(vao_id);
    glEnableVertexAttribArray(1); 
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices_buffer);
    glVertexAttribPointer(
       1,                  
       1,                  // size
       GL_UNSIGNED_INT,     // type
       GL_FALSE,     // normalized?
       0,                  // stride
       (void*)0        // array buffer offset
    );
    glBindVertexArray(0);
}

void Mesh::loadGeometry(Vector3* v_, unsigned int nb_vertices_)
{
	if (v_ != NULL)
	{
		for(unsigned int i=0; i<nb_vertices_;i++)
		{
			mesh_box.addPoint(v_[i]);
		}
	}
    
    glGenBuffers(1, &geometry_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, geometry_buffer);
    glBufferData(GL_ARRAY_BUFFER, nb_vertices_*3*sizeof(float), v_, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    nb_vertices = nb_vertices_;
    
    glBindVertexArray(vao_id);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, geometry_buffer);
    glVertexAttribPointer(
       0,                  
       3,                  // size
       GL_FLOAT,     // type
       GL_FALSE,     // normalized?
       0,                  // stride
       (void*)0        // array buffer offset
    );
    glBindVertexArray(0);
}

void Mesh::addBuffer(int nb_elements, float groups, float size_of, GLenum type, void* data)
{
    data_buffers.push_back(0);
    
    glGenBuffers(1, &data_buffers[nb_buffers]);
    glBindBuffer(GL_ARRAY_BUFFER, data_buffers[nb_buffers]);
    glBufferData(GL_ARRAY_BUFFER, nb_elements*groups*size_of, data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(vao_id);
    glEnableVertexAttribArray(nb_buffers+2); 
    glBindBuffer(GL_ARRAY_BUFFER, data_buffers[nb_buffers]);
    glVertexAttribPointer(
       nb_buffers+2,                  
       groups,          // size
       type,             // type
       GL_FALSE,     // normalized?
       0,                  // stride
       (void*)0        // array buffer offset
    );
    glBindVertexArray(0);
    
    nb_buffers++;
}

void Mesh::bindIt()
{
    glBindVertexArray(vao_id);
}
void Mesh::unbindIt()
{
    glBindVertexArray(0);
}

void Mesh::drawMesh(GLenum primitive)
{
	
    if (nb_indices > 0)
        drawElements(primitive);
    else
        drawArrays(primitive);
	
}

void Mesh::drawArrays(GLenum primitive)
{
	bindIt();
    glDrawArrays(primitive, 0, nb_vertices);
	unbindIt();
}

void Mesh::drawElements(GLenum primitive)
{
	bindIt();
    if (nb_indices <= 0)
    {
        fprintf(stderr, "Asking for indexed drawing but no indices to draw from\n Drawing Arrays instead\n");
        drawArrays(primitive);
        return;
    }
    glDrawElements(primitive, nb_indices, GL_UNSIGNED_INT, 0);
	unbindIt();
}

/** DEBUG**/

void Mesh::loadTestPlane(float size)
{
    Vector3 triangle_vertices[] = {
        Vector3( -size, 0.0f, -size),
        Vector3(size, 0.0f, -size),
        Vector3(size, 0.0f, size),
        Vector3(-size, 0.0f, size)
    };
    loadGeometry(triangle_vertices, 4);
    
    unsigned int triangle_indices[] = {
        0,
        1,
        2,
        2,
        3,
        0
    };
    loadIndices(triangle_indices, 6);
    
    Vector3 triangle_normals[] = {
        Vector3(0.0, 1.0, 0.0),
        Vector3(0.0, 1.0, 0.0),
        Vector3(0.0, 1.0, 0.0),
        Vector3(0.0, 1.0, 0.0)
    };
    addBuffer(4, 3, sizeof(float), GL_FLOAT, &triangle_normals[0]);
    
    Vector2 triangle_textures[] = {
        Vector2(0.0, 0.0),
        Vector2(1.0, 0.0),
        Vector2(1.0, 1.0),
        Vector2(0.0, 1.0)
    };
    addBuffer(4, 2, sizeof(float), GL_FLOAT, &triangle_textures[0]);
    
     Vector3 triangles_tan[] = {
        Vector3(1.0, 0.0, 0.0),
        Vector3(1.0, 0.0, 0.0),
        Vector3(1.0, 0.0, 0.0),
        Vector3(1.0, 0.0, 0.0)
    };
    addBuffer(4, 3, sizeof(float), GL_FLOAT, &triangles_tan[0]);
    
     Vector3 triangles_bitan[] = {
        Vector3(0.0, 0.0, 1.0),
        Vector3(0.0, 0.0, 1.0),
        Vector3(0.0, 0.0, 1.0),
        Vector3(0.0, 0.0, 1.0)
    };
    addBuffer(4, 3, sizeof(float), GL_FLOAT, &triangles_bitan[0]);
    
}

void Mesh::loadEmptyVertexArray(int size)
{
    loadGeometry(NULL, size);   
}


void Mesh::loadTestGrid(int nbx, int nby, float sx, float sy)
{
    Vector3* triangle_vertices = new Vector3 [nbx*nby];
    unsigned int* triangle_indices = new unsigned int[nbx*nby*6];
    Vector2* triangle_texture = new Vector2[nbx*nby*6];

    float stepx = sx/nbx;
    float stepy = sy/nby;
    
    int nb = 0;
    for(int i=0; i<nbx; i++)
    for(int j=0; j<nby; j++)
    {
        triangle_vertices[nb] = Vector3(i*stepx, 0, j*stepy);
        triangle_texture[nb++] = Vector2((float)i/(float)nbx, (float)j/(float)nby);
    }
    
    loadGeometry(triangle_vertices, nbx*nby);
    
    nb =0;
    for(int i=0; i<nbx-1; i++)
    for(int j=0; j<nby-1; j++)
    {
        triangle_indices[nb++] = (i)*nby + j;
        triangle_indices[nb++] = (i)*nby + j + 1;
        triangle_indices[nb++] =  (i+1)*nby + j;
        
        triangle_indices[nb++] = (i+1)*nby + j+1;
        triangle_indices[nb++] =  (i+1)*nby + j;
        triangle_indices[nb++] = (i)*nby + j + 1;
    }
    
    loadIndices(triangle_indices, (nbx-1)*(nby-1)*6);
    addBuffer(nbx*nby, 2, sizeof(float), GL_FLOAT, &triangle_texture[0]);
    
    delete [] (triangle_vertices);
    delete [] (triangle_indices);
    delete [] (triangle_texture);
}

void Mesh::loadFullSquare()
{
	Vector3* triangle_vertices = new Vector3[4];
	
	triangle_vertices[0] = Vector3(-1, -1, 0);
	triangle_vertices[1] = Vector3(-1, 1, 0);
	triangle_vertices[2] = Vector3(1, -1, 0);
	triangle_vertices[3] = Vector3(1, 1, 0);
	
	loadGeometry(triangle_vertices, 4);
	
	delete [] (triangle_vertices);
}

void Mesh::loadScreenSpaceGrid(int N, int M, float coverage)
{
	Vector3* triangle_vertices = new Vector3[N*M];
	unsigned int* triangle_indices = new unsigned int[N*M*6];
   
	float stepx = 2.0/(N-1);
	float stepy = (2.0*coverage)/(M-1);
    
	int nb = 0;
	for(float i=0; i<N; i++)
	for(float j=0; j<M; j++)
	{
		triangle_vertices[nb] = Vector3(i*stepx - 1.0, j*stepy - 1.0, -1.0);
		nb++;
	}
    
	loadGeometry(triangle_vertices, N*M);
    
	nb =0;
	for(int i=0; i<N-1; i++)
	for(int j=0; j<M-1; j++)
	{
		triangle_indices[nb++] = (i)*M + j;
		triangle_indices[nb++] = (i)*M + j + 1;
		triangle_indices[nb++] =  (i+1)*M + j;
		
		triangle_indices[nb++] = (i+1)*M + j+1;
		triangle_indices[nb++] =  (i+1)*M + j;
		triangle_indices[nb++] = (i)*M + j + 1;
	}
	
	loadIndices(triangle_indices, (N-1)*(M-1)*6);
	
	delete [] (triangle_indices);
	delete [] (triangle_vertices);
}



void Mesh::rotateOnMouse(GLWindow* w)
{
        if (w->isClikedLeft())
        {
            double x, y;
            w->getMouseMove(&x, &y);
            rotateX(y*0.005);
            rotateY(x*0.005);
        }
}

bool Mesh::loadMesh0FromFile(const char* file)
{
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile( file,
	aiProcess_CalcTangentSpace |
	aiProcess_Triangulate |
	aiProcess_JoinIdenticalVertices |
	aiProcess_GenSmoothNormals
	);

	if(!scene)
	{
		printf("Cannot load %s : %s\n", file, importer.GetErrorString());
		return false;
	}
	if (!scene->HasMeshes())
	{
		printf("No mesh in file %s\n", file);
		return false;
	}
	
	printf("Loading mesh %s \n", file);
	
	aiMesh* m = scene->mMeshes[0];
	
	loadFromData(m);
	
	return true;
}

bool Mesh::loadFromData(aiMesh* m)
{
	printf("Geometry ...\n");
	loadGeometry((Vector3*)m->mVertices, m->mNumVertices);
	for (unsigned int v = 0; v < m->mNumVertices; ++v) {
		mesh_box.addPoint(Vector3(m->mVertices[v].x, m->mVertices[v].y, m->mVertices[v].z));
	}
	
	printf("Connectivity ...\n");
	std::vector<unsigned int> indices;
	for (unsigned int t = 0; t < m->mNumFaces; ++t) {
		aiFace* face = &m->mFaces[t];
		for(unsigned int i = 0; i < face->mNumIndices; i++)
			indices.push_back(face->mIndices[i]);
	}
	loadIndices(&indices[0], indices.size());
	
	
	if (m->HasNormals())
	{
		printf("Normals ...\n");
		addBuffer(m->mNumVertices, 3, sizeof(float), GL_FLOAT, (Vector3*)m->mNormals);
	}
	
	if (m->HasTextureCoords(0))
	{
		printf("Texture Coords ...\n");
		std::vector<Vector2> texcoords;
		for(unsigned int i = 0; i< m->mNumVertices; i++)
		{
			aiVector3D tc3D = m->mTextureCoords[0][i];
			texcoords.push_back(Vector2(tc3D.x, tc3D.y));
		}
		addBuffer(texcoords.size(), 2, sizeof(float), GL_FLOAT, &texcoords[0]);
	}
	
	if (m->HasTangentsAndBitangents())
	{
		printf("Tangents and Bitangents ...\n");
		addBuffer(m->mNumVertices, 3, sizeof(float), GL_FLOAT, (Vector3*)m->mTangents);
		addBuffer(m->mNumVertices, 3, sizeof(float), GL_FLOAT, (Vector3*)m->mBitangents);
	}
	
	return true;
}



/***
 
  
void compute_tangents_bitangents(const std::vector<Vector3>& vertices_buffer, const std::vector<Vector2>& textures_buffer, std::vector<Vector3> & tangents, std::vector<Vector3> & bitangents)
{
    for(unsigned int i=0; i<vertices_buffer.size(); i+=3)
    {
        Vector3 v0 = vertices_buffer[i];
        Vector3 v1 = vertices_buffer[i+1];
        Vector3 v2 = vertices_buffer[i+2];
        
        Vector2 uv0 = textures_buffer[i];
        Vector2 uv1 = textures_buffer[i+1];
        Vector2 uv2 = textures_buffer[i+2];
        
        Vector3 delta_pos1 = v1 - v0;
        Vector3 delta_pos2 = v2 - v0;
        
        Vector2 delta_uv1 = uv1 - uv0;
        Vector2 delta_uv2 = uv2 - uv0;
        
        float r = 1.0f / (delta_uv1.x() * delta_uv2.y() - delta_uv1.y() * delta_uv2.x());
        Vector3 T = (delta_pos1 * delta_uv2.y()   - delta_pos2 * delta_uv1.y())*r;
        Vector3 B = (delta_pos2 * delta_uv1.x()   - delta_pos1 * delta_uv2.x())*r;
        
        tangents.push_back(T);
        tangents.push_back(T);
        tangents.push_back(T);
        
        bitangents.push_back(B);
        bitangents.push_back(B);
        bitangents.push_back(B);
    }
    
    
}

void Mesh::loadFromObj(const char* file)
{
    std::vector<Vector3> read_vertices;
    std::vector<Vector3> read_normals;
    std::vector<Vector2> read_textures;
    
    std::vector<Vector3> vertices_buffer;
    std::vector<Vector3> normals_buffer;
    std::vector<Vector2> textures_buffer;
    
    std::vector<Vector3> tangents;
    std::vector<Vector3> bitangents;
    
    //std::vector<unsigned int> indices;
    
    FILE* f = fopen(file, "r");
    if (!f)
    {
        fprintf(stderr, "File not found %s\n", file);
        return;
    }

    char line[256];
    fgets(line, 256, f);
    
    while ( !feof(f) )
    {
        if  ( (line[0] == '/' && line[1] == '/') ||
               (line[0] == '\0') ||
               (line[0] == '\n')
            )
                break;

        switch(line[0])
        {
                case 'v':
                {
                    switch(line[1])
                    {
                        case ' ' :
                        {
                            double x, y, z;
                            sscanf(line, "%*c %lf %lf %lf\n", &x, &y, &z);
                            read_vertices.push_back(Vector3(x, y, z));
                            break;
                        }
                        case 'n':
                        {
                            double nx, ny, nz;
                            sscanf(line, "%*c%*c %lf %lf %lf\n", &nx, &ny, &nz);
                            read_normals.push_back(Vector3(nx, ny, nz));
                            break;
                        }
                        case 't':
                        {
                            double tx, ty;
                            sscanf(line, "%*c%*c %lf %lf\n", &tx, &ty);
                            
                            if (tx > 1)
                                tx -= 1;
                            if (ty > 1)
                                ty -= 1;
                        
                            read_textures.push_back(Vector2(tx, ty));
                            break;
                        }
                    }
                    break;
                }
                case 'f':
                {
                    unsigned int a, b, c;
                    unsigned int na, nb, nc;
                    unsigned int ta, tb, tc;
                    if (read_normals.size() != 0)
                    {
                        if (read_textures.size() != 0)
                        {
                            sscanf(line, "%*c %u/%u/%u %u/%u/%u %u/%u/%u\n", &a, &ta, &na, &b, &tb, &nb, &c, &tc, &nc);
                            
                            textures_buffer.push_back(read_textures[ta-1]);
                            textures_buffer.push_back(read_textures[tb-1]);
                            textures_buffer.push_back(read_textures[tc-1]);
                        }
                        else
                            sscanf(line, "%*c %u//%u %u//%u %u//%u\n", &a, &na, &b, &nb, &c, &nc);

                        normals_buffer.push_back(read_normals[na-1]);
                        normals_buffer.push_back(read_normals[nb-1]);
                        normals_buffer.push_back(read_normals[nc-1]);
                    }
                    else
                    {
                        if (read_textures.size() != 0)
                        {
                            sscanf(line, "%*c %u/%u %u/%u %u/%u\n", &a, &ta, &b, &tb, &c, &tc);
                         
                            textures_buffer.push_back(read_textures[ta-1]);
                            textures_buffer.push_back(read_textures[tb-1]);
                            textures_buffer.push_back(read_textures[tc-1]);
                        }
                        else
                            sscanf(line, "%*c %u %u %u\n", &a, &b, &c);
                    }
                    
                    vertices_buffer.push_back(read_vertices[a-1]);
                    vertices_buffer.push_back(read_vertices[b-1]);
                    vertices_buffer.push_back(read_vertices[c-1]);
                    
                    break;
                }
                default:
                    printf("%s", line);
                    break;
        }
        
        fgets(line, 256, f);
    }

    fclose(f);
    
    loadGeometry(&(vertices_buffer[0]), vertices_buffer.size());
    //if (indices.size() != 0)
    //    loadIndices(&(indices[0]), indices.size());
    if (normals_buffer.size() != 0)
        addBuffer(normals_buffer.size(), 3, sizeof(float), GL_FLOAT, &normals_buffer[0]);
    if (textures_buffer.size() != 0)
        addBuffer(textures_buffer.size(), 2, sizeof(float), GL_FLOAT, &textures_buffer[0]);
    if (normals_buffer.size() != 0 && textures_buffer.size() != 0)
    {
        tangents.reserve(vertices_buffer.size());
        bitangents.reserve(vertices_buffer.size());
        
        compute_tangents_bitangents(vertices_buffer, textures_buffer, tangents, bitangents);
        
        addBuffer(tangents.size(), 3, sizeof(float), GL_FLOAT, &tangents[0]);
        addBuffer(bitangents.size(), 3, sizeof(float), GL_FLOAT, &bitangents[0]);
    }
    
    printf("Read %s with %lu vertices, %lu faces, %lu normals, %lu texcoords\n\n", file, read_vertices.size(), vertices_buffer.size()/3, read_normals.size(), read_textures.size());
}

***/
