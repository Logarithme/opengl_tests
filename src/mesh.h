#ifndef MESH_H_
#define MESH_H_

#include "geo_math.h"
#include "window.h"
#include "texture.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h" 

#include <GL/glew.h>
#include <GL/gl.h>
#include <vector>
#include <boost/concept_check.hpp>

class BBox
{
public:
    
	Vector3 pMin;
	Vector3 pMax;

	BBox()
	{
		pMin = Vector3(0, 0, 0);
		pMax = Vector3(0, 0, 0);
	}
	BBox(Vector3 min_point, Vector3 max_point)
	{
		pMin = min_point;
		pMax = max_point;
	}
	void addPoint(Vector3 v)
	{
		if (v.x() > pMax.x())
			pMax.x(v.x());
		if (v.y() > pMax.y())
			pMax.y(v.y());
		if (v.z() > pMax.z())
			pMax.z(v.z());

		if (v.x() < pMin.x())
			pMin.x(v.x());
		if (v.y() < pMin.y())
			pMin.y(v.y());
		if (v.z() < pMin.z())
			pMin.z(v.z());
	}
	void addBox(BBox b)
	{
		addPoint(b.pMax);
		addPoint(b.pMin);
	}
	Vector3 getSizes()
	{
		return Vector3(pMax - pMin);
	}
	Vector3 getMidPoint()
	{
		return (pMax - pMin)*0.5 + pMin;
	}
	bool isInside(Vector3 v)
	{
		if (
			(v.x() > pMax.x())
			|| (v.y() > pMax.y())
			|| (v.z() > pMax.z())
			|| (v.x() < pMin.x())
			|| (v.y() < pMin.y())
			|| (v.z() < pMin.z())
			)
			return false;
		else
			return true;
	}
    
};

class Mesh
{
	
private:
    
	/**Data**/
	unsigned int nb_buffers;
	unsigned int nb_vertices;
	unsigned int nb_indices;
	BBox mesh_box;

	/**Rendering**/
	GLuint indices_buffer;
	GLuint geometry_buffer;
	std::vector<GLuint> data_buffers;
	GLuint vao_id;
	bool vao_loaded;
	
	Texture2D texture;

	/**Moving**/
	Matrix44 model;
    
public:
	Mesh();
	Mesh(const char* file);
	
	bool loadMesh0FromFile(const char* file);
	bool loadFromData(aiMesh* m);

	//void loadFromObj(const char* file);
	void loadIndices(unsigned int* data, unsigned int nb_indices);
	void loadGeometry(Vector3* data, unsigned int nb_vertices);

	void addBuffer(int nb_elements, float groups, float size_of, GLenum type, void* data);

	void loadTestPlane(float s=100.0);
	void loadTestGrid(int nbx, int nby, float sx=100.0, float sy=100.0);
	void loadFullSquare();
	void loadScreenSpaceGrid(int N=20, int M=10, float coverage = 1.0);
	void loadEmptyVertexArray(int size);

	void bindIt();
	void unbindIt();

	void drawMesh(GLenum primitive);
	void drawArrays(GLenum primitive);
	void drawElements(GLenum primitive);

	Matrix44 getMatrix() { return model; }

	void rotateX(float angle) { model = RotateX(angle) * model; }
	void rotateY(float angle) { model = RotateY(angle) * model; }
	void rotateZ(float angle) { model = RotateZ(angle) * model; }
	
	void scale(Vector3 scale) { model = Scale(scale) * model; }

	BBox box() { return mesh_box; }

	void rotateOnMouse(GLWindow* w);
};

#endif
