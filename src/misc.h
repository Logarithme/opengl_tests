#ifndef OPENGL_UTILS_H_
#define OPENGL_UTILS_H_

#include <cstdio>
#include <cstring>

#include "texture.h"
#include "CImg.h"

#define SCREENSHOT_PATH(x) "../screenshots/" x

bool queryExtension(const char* ext)
{
    printf("Extension %s ... ", ext);
    GLint n, i;
    glGetIntegerv(GL_NUM_EXTENSIONS, &n);
    for (i = 0; i < n; i++) 
    {
        const unsigned char* current = glGetStringi(GL_EXTENSIONS, i);
        if (strcmp((const char*)current, ext) == 0)
        {
            printf("found\n");
            return true;
        }
    }
    printf("NOT FOUND\n");
    return false;
}

const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y%m%d_%Hh%Mm%Ss", &tstruct);

    return buf;
}

void capture(std::string name=NULL, int w=1024, int h=768)
{
	if (name.empty())
	{
		name = SCREENSHOT_PATH("capture_" + currentDateTime() + ".bmp");
	}
	
	unsigned char* data = (unsigned char*)malloc(w*h*3*sizeof(unsigned char));
	
	glReadPixels(0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, data);
	
	cimg_library::CImg<unsigned char> image(w, h, 1, 3, 0);
	
	int indice = 0;
	for(int j=h-1; j>=0; j--)
	for(int i=0; i<w; i++)
	for(int c=0; c<3; c++)
	{
		*image.data(i, j, 0, c) = data[indice];
		indice++;
	}
	
	printf("Saving to %s\n", name.c_str());
	image.save(name.c_str());
	//image.display();
}

Texture2D load2DTexture(std::string tex_file, bool repeat=false)
{
    Texture2D t(tex_file.c_str(), GL_UNSIGNED_BYTE, repeat);
    t.generateMipmap();
    t.activateTexture();
    return t;
}

Texture2D loadEmpty2DTexture(unsigned int w, unsigned int h, GLenum type=GL_UNSIGNED_BYTE, int chan=3, bool repeat = 0)
{
    Texture2D t;
    t.loadTextureData(NULL, w, h, GL_UNSIGNED_BYTE, 3, repeat );
    t.generateMipmap();
    t.activateTexture();
    return t;
}

Program* makeProgramFrom(char* file)
{
    Program* p = new Program();
    p->readSource(file);
    p->compile();
    p->link();
    return p;
}

#endif
