#include "num_math.h"

#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstdio>

double Complex::getModule()
{
	return sqrt(re*re + im*im);
}
double Complex::getArgument()
{
	if (re == 0 && im == 0)
		return 0;
	
	return atan2(im, re);
}

void Complex::fromModuleArgument(double r, double t)
{
	re = r*cos(t);
	im = r*sin(t);
}

Complex Complex::getConjugate()
{
	return Complex(re, -im);
}

bool Complex::operator==(Complex c)
{
	return (re == c.real() && im == c.imaginary());
}

Complex gaussianRandom()
{	
	float x1, x2, w, y1, y2;
 
         do {
                 x1 = 2.0 * ((float)rand()/(float)RAND_MAX) - 1.0;
                 x2 = 2.0 * ((float)rand()/(float)RAND_MAX) - 1.0;
                 w = x1 * x1 + x2 * x2;
         } while ( w >= 1.0 );
	 

         w = sqrt( (-2.0 * log( w ) ) / w );
         y1 = x1 * w;
         y2 = x2 * w;
	 
	 return Complex(y1, y2);
}

