#ifndef NUM_MATH_H_
#define NUM_MATH_H_

#include <cstdio>

class Complex
{
private:
	double re; //real
	double im; //imaginary
public:
	Complex() { re=0; im=0; }
	//Complex(double x) { re = x; im = 0; }
	Complex(double x, double y) { re = x; im = y; }
	Complex(double xy[2]) { re = xy[0]; im = xy[1]; }
	
	void fromModuleArgument(double, double);
	
	void real(double x) { re = x; }
	void imaginary(double y) { im = y; }
	double real() { return re; }
	double imaginary() { return im; }
	
	Complex getConjugate();
	
	double getModule();
	double getArgument();
	
	bool operator==(Complex c);
	Complex operator+(Complex c) { return Complex( re + c.real(),  im + c.imaginary()); }
	Complex operator+=(Complex c) { re += c.real(); im += c.imaginary(); return *this; }
	Complex operator-(Complex c) { return Complex( re - c.real(), im - c.imaginary()); }
	Complex operator*(Complex c) { return Complex( re*c.real() - im*c.imaginary(), re*c.imaginary() + im*c.real()); }
	Complex operator*(double c) { return Complex(re*c, im*c); }
	Complex operator/(double c) { return Complex(re/c, im/c); }
	
	void print() { printf("(%f, %f)\n", re, im); }
};

Complex gaussianRandom(); //Box Muller Polar Form

#endif