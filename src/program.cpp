#include "program.h"

#include <fstream>
#include <vector>
#include <cstring>
#include <cstdlib>

Program::Program() : shader_ids((int)shader_count), sources((int)shader_count)
{
	program_id = glCreateProgram();
	has_vertex = false;
	has_fragment = false;
	has_geometry = false;
}
Program::~Program()
{
	printf("Cleaning Program %d ...\n", program_id);

	if (has_vertex)
		glDeleteShader(shader_ids[vertex]);
	if (has_fragment)
		glDeleteShader(shader_ids[fragment]);
	if (has_geometry)
		glDeleteShader(shader_ids[geometry]);
	
	glDeleteProgram(program_id);
}

void Program::readSource(const char* file)
{
	printf("Reading %s\n", file);

	if (source_file.empty())
		source_file = file;

	std::string version;
	std::ifstream s_stream(file, std::ios::in);

	Shader current_shader = shader_count;

	std::string path = file;
	path = path.substr(0, path.find_last_of("/")+1);

	int ln = 0;
	if( ! s_stream.is_open() )
		printf("Error opening %s\n", file);
	else
	{
		std::string line = "";
		while(getline(s_stream, line))
		{
			if (line[0] == '#')
			{
				if (line.find("#version") != std::string::npos)
				{
				version = line + "\n";
				}
			}
			
			if (line[0] == '$')
			{
				if (line.find("$include") != std::string::npos)
				{
					std::string inc_file = line.substr(9);
					if (current_shader != shader_count)
					{
						printf("Including file %s\n", inc_file.c_str());
						std::ifstream s_stream2((path + inc_file).c_str(), std::ios::in);
						if(s_stream2.is_open())
						{
							std::string line2 = "";
							while(getline(s_stream2, line2))
								sources[(int)current_shader]  += "\n" + line2;
							s_stream2.close();
						}
					}
				}   
				else if (line.find("VERTEX_SHADER") != std::string::npos)
				{
					has_vertex = true;
					if (line.find("$start") != std::string::npos)
					{
						shader_ids[vertex] = glCreateShader(GL_VERTEX_SHADER);
						printf("Vertex Shader starts line %d\n", ln);
						current_shader = vertex;
					}
					else if (line.find("$end") !=std::string::npos)
					{
						printf("Vertex Shader ends line %d\n", ln);
						current_shader = shader_count;
					}
				}
				else if (line.find("FRAGMENT_SHADER") != std::string::npos)
				{
					has_fragment = true;
					if (line.find("$start") != std::string::npos)
					{
						shader_ids[fragment] = glCreateShader(GL_FRAGMENT_SHADER);
						printf("Fragment Shader starts line %d\n", ln);
						current_shader = fragment;
					}
					else if (line.find("$end") != std::string::npos)
					{
						printf("Fragment Shader ends line %d\n", ln);
						current_shader = shader_count;
					}
				}
				else if (line.find("GEOMETRY_SHADER") != std::string::npos)
				{
					has_geometry = true;
					if (line.find("$start") != std::string::npos)
					{
						shader_ids[geometry] = glCreateShader(GL_GEOMETRY_SHADER);
						printf("Geometry Shader starts line %d\n", ln);
						current_shader = geometry;
					}
					else if (line.find("$end") !=std::string::npos)
					{
						printf("Geometry Shader ends line %d\n", ln);
						current_shader = shader_count;
					}
				}
			}
			else
			{
				if (current_shader != shader_count)
				{
					sources[(int)current_shader] += "\n" + line;
				}
			}
			ln++;
		}
		s_stream.close();
	}
    
	for(int i=0; i<(int)shader_count; i++)
	{
		if (!sources[i].empty())
		{
			sources[i] = version + sources[i];
		}
	}
}

void Program::compileSource(GLuint shader_id, const char* source)
{
	GLint res = GL_FALSE;
	int log_length;

	glShaderSource(shader_id, 1, &source , NULL);
	glCompileShader(shader_id);

	// Check Shader
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &res);
	glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);
	std::vector<char> s_error(log_length);
	glGetShaderInfoLog(shader_id, log_length, NULL, &s_error[0]);
	fprintf(stdout, "%s\n", &s_error[0]);
}

void Program::compile()
{
	if (has_vertex)
	{
		compileSource(shader_ids[vertex],  sources[vertex].c_str());
		glAttachShader(program_id, shader_ids[vertex]);
	}
	
	if (has_fragment)
	{
		compileSource(shader_ids[fragment], sources[fragment].c_str());
		glAttachShader(program_id, shader_ids[fragment]);
	}
	
	if (has_geometry)
	{
		compileSource(shader_ids[geometry], sources[geometry].c_str());
		glAttachShader(program_id, shader_ids[geometry]);
	}
}

void Program::link()
{
    GLint res = GL_FALSE;
    int log_length=0;
    
    fprintf(stdout, "Linking program %d\n", program_id);
    glLinkProgram(program_id);
    
    glGetProgramiv(program_id, GL_LINK_STATUS, &res);
    glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_length);
    std::vector<char> p_error( std::max(log_length+1, int(1)) );
    glGetProgramInfoLog(program_id, log_length+1, NULL, &p_error[0]);
    fprintf(stdout, "%s\n", &p_error[0]);
	if (strlen(&p_error[0]) != 0)
		exit(-1);
}

void Program::use()
{
	glUseProgram(program_id);
}
void Program::unuse()
{
	glUseProgram(0);
}

void Program::setUniform(const char* name, Matrix44 m)
{
	GLuint loc = glGetUniformLocation(program_id, name);
	glProgramUniformMatrix4fv(program_id, loc, 1, GL_FALSE, m.transpose().data());
	uniforms_matrix44.insert(std::pair<std::string, Matrix44>(name, m));
}

void Program::setUniform(const char* name, Vector3 v)
{
	GLuint loc = glGetUniformLocation(program_id, name);
	glProgramUniform3f(program_id, loc, v.x(), v.y(), v.z());
	uniforms_vector3.insert(std::pair<std::string, Vector3>(name, v));
}

void Program::setUniform(const char* name, Vector4 v)
{
	GLuint loc = glGetUniformLocation(program_id, name);
	glProgramUniform4f(program_id, loc, v.x(), v.y(), v.z(), v.w());
	uniforms_vector4.insert(std::pair<std::string, Vector4>(name, v));
}

void Program::setUniform(const char* name, unsigned int e)
{
	GLuint loc = glGetUniformLocation(program_id, name);
	glProgramUniform1i(program_id, loc, e);
	uniforms_ui.insert(std::pair<std::string, unsigned int>(name, e));
}

void Program::setUniform(const char* name, float e)
{
	GLuint loc = glGetUniformLocation(program_id, name);
	glProgramUniform1f(program_id, loc, e);
	uniforms_float.insert(std::pair<std::string, float>(name, e));
}

void Program::setUniform(const char* name, bool e)
{
	GLuint loc = glGetUniformLocation(program_id, name);
	if (e)
		glProgramUniform1i(program_id, loc, 1);
	else
		glProgramUniform1i(program_id, loc, 0);
	uniforms_bool.insert(std::pair<std::string, bool>(name, e));
}

void Program::setUniform(const char* name, int e)
{
	GLuint loc = glGetUniformLocation(program_id, name);
	glProgramUniform1i(program_id, loc, e);
	uniforms_int.insert(std::pair<std::string, int>(name, e));
}

void Program::setUniform(const char* name, Vector2 v)
{
	GLuint loc = glGetUniformLocation(program_id, name);
	glProgramUniform2f(program_id, loc, v.x(), v.y());
	uniforms_vector2.insert(std::pair<std::string, Vector2>(name, v));
}


void Program::reload()
{
	for(unsigned int i=0; i<sources.size(); i++)
		sources[i].clear();
    
	for(unsigned int i=0; i<shader_ids.size(); i++)
		glDeleteShader(shader_ids[i]);
    
	glDeleteProgram(program_id);
	program_id = glCreateProgram();

	readSource(source_file.c_str());
	compile();
	link();

	for (std::map<std::string,Matrix44>::iterator it=uniforms_matrix44.begin(); it!=uniforms_matrix44.end(); ++it)
		setUniform(((std::string)(it->first)).c_str(), it->second);

	for (std::map<std::string, Vector2>::iterator it=uniforms_vector2.begin(); it!=uniforms_vector2.end(); ++it)
		setUniform(((std::string)(it->first)).c_str(), it->second);

	for (std::map<std::string, Vector3>::iterator it=uniforms_vector3.begin(); it!=uniforms_vector3.end(); ++it)
		setUniform(((std::string)(it->first)).c_str(), it->second);

	for (std::map<std::string, Vector4>::iterator it=uniforms_vector4.begin(); it!=uniforms_vector4.end(); ++it)
		setUniform(((std::string)(it->first)).c_str(), it->second);

	for (std::map<std::string,unsigned int>::iterator it=uniforms_ui.begin(); it!=uniforms_ui.end(); ++it)
		setUniform(((std::string)(it->first)).c_str(), it->second);
		
	for (std::map<std::string,float>::iterator it=uniforms_float.begin(); it!=uniforms_float.end(); ++it)
		setUniform(((std::string)(it->first)).c_str(), it->second);

	for (std::map<std::string,int>::iterator it=uniforms_int.begin(); it!=uniforms_int.end(); ++it)
		setUniform(((std::string)(it->first)).c_str(), it->second);

	for (std::map<std::string,bool>::iterator it=uniforms_bool.begin(); it!=uniforms_bool.end(); ++it)
		setUniform(((std::string)(it->first)).c_str(), it->second);
}


ComputeProgram::ComputeProgram() : Program()
{
}

void ComputeProgram::readSource(const char* file)
{
	printf("Reading %s\n", file);

	if (source_file.empty())
	source_file = file;

	std::string version;
	std::ifstream s_stream(file, std::ios::in);

	bool shader_code = false;

	std::string path = file;
	path = path.substr(0, path.find_last_of("/")+1);

	int ln = 0;
	if( ! s_stream.is_open() )
		printf("Error opening %s\n", file);
	else
	{
		std::string line = "";
		while(getline(s_stream, line))
		{
			if (line[0] == '#')
			{
				if (line.find("#version") != std::string::npos)
				{
				version = line + "\n";
				}
			}
			
			if (line[0] == '$')
			{
				if (line.find("$include") != std::string::npos)
				{
					std::string inc_file = line.substr(9);
					if (shader_code)
					{
						printf("Including file %s\n", inc_file.c_str());
						std::ifstream s_stream2((path + inc_file).c_str(), std::ios::in);
						if(s_stream2.is_open())
						{
							std::string line2 = "";
							while(getline(s_stream2, line2))
								sources[0]  += "\n" + line2;
							s_stream2.close();
						}
					}
				}   
				else if (line.find("COMPUTE_SHADER") != std::string::npos)
				{
					if (line.find("$start") != std::string::npos)
					{
						shader_ids[0] = glCreateShader(GL_COMPUTE_SHADER);
						printf("Compute Shader starts line %d\n", ln);
					}
					else if (line.find("$end") !=std::string::npos)
					{
						printf("Compute Shader ends line %d\n", ln);
					}
					shader_code = true;
				}
			}
			else
			{
				if (shader_code)
				{
					sources[0] += "\n" + line;
				}
			}
			ln++;
		}
		s_stream.close();
	}

	sources[0] = version + sources[0];
}

void ComputeProgram::compile()
{
	compileSource(shader_ids[0],  sources[0].c_str());
	glAttachShader(program_id, shader_ids[0]);
}

void ComputeProgram::dispatch(int x, int y, int z)
{
	glDispatchCompute(x, y, z);
}




