#ifndef PROGRAM_H_
#define PROGRAM_H_

#include <GL/glew.h>
#include <GL/gl.h>
#include <string>
#include <vector>
#include <map>

#include "geo_math.h"

#include <cstdio>

enum Shader
{
	vertex,
	fragment,
	geometry,
	compute,
	shader_count
};

class Program
{
	
protected:
	GLuint program_id;
	std::string source_file;

	std::vector<GLuint> shader_ids;
	std::vector<std::string> sources;

	std::map< std::string, Matrix44 > uniforms_matrix44;
	std::map< std::string, Vector2 > uniforms_vector2;
	std::map< std::string, Vector3 > uniforms_vector3;
	std::map< std::string, Vector4 > uniforms_vector4;
	std::map< std::string, unsigned int > uniforms_ui;
	std::map< std::string, float > uniforms_float;
	std::map< std::string, int > uniforms_int;
	std::map< std::string, bool > uniforms_bool;

	void compileSource(GLuint shader_id, const char* source);
    
private:
	bool has_vertex;
	bool has_fragment;
	bool has_geometry;
    
public:
	Program();
	~Program();
	
	void readSource(const char* file);
	void setSource(const char* vertex_source, const char* fragment_source)
	{
		if (vertex_source != NULL)
		{
			shader_ids[vertex] = glCreateShader(GL_VERTEX_SHADER);
			sources[vertex] = vertex_source;
			has_vertex = true;
			//printf("%s\n", sources[vertex].c_str());
		}
		if (fragment_source != NULL)
		{
			shader_ids[fragment] = glCreateShader(GL_FRAGMENT_SHADER);
			sources[fragment] = fragment_source;
			has_fragment= true;
			//printf("%s\n", sources[fragment].c_str());
		}
	}

	void reload();

	void compile();
	void link();

	void use();
	void unuse();

	void setUniform(const char* name, Matrix44);
	void setUniform(const char* name, Vector2);
	void setUniform(const char* name, Vector3);
	void setUniform(const char* name, Vector4);
	void setUniform(const char* name, unsigned int);
	void setUniform(const char* name, float);
	void setUniform(const char* name, bool);
	void setUniform(const char* name, int e);
};

class ComputeProgram : public Program
{
	
public:
	ComputeProgram();
	
	void readSource(const char* file);
	void compile();
	
	void dispatch(int x=32, int y=32, int z=32);
};

#endif
