#include "query.h"

TimeQuery::TimeQuery()
{
	glGenQueries(1, &id);
}

TimeQuery::~TimeQuery()
{
	glDeleteQueries(1, &id);
}
void TimeQuery::start()
{
	glBeginQuery(GL_TIME_ELAPSED, id);
}
void TimeQuery::end()
{
	glEndQuery(GL_TIME_ELAPSED);
}
GLuint64 TimeQuery::getResult()
{
	GLuint64 r;
	glGetQueryObjectui64v(id, GL_QUERY_RESULT, &r) ;
	return r;
}



