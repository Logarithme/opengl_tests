#ifndef QUERY_H_
#define QUERY_H_

#include <GL/glew.h>
#include <GL/gl.h>

class TimeQuery
{
public:
	TimeQuery();
	~TimeQuery();
	
	GLuint64 getResult();
	void start();
	void end();
	
private:
	
	GLuint id;
};

#endif