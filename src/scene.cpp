#include "scene.h"

Scene::Scene()
{
	num_meshes = 0;
	mesh_list = 0;
}


Scene::Scene(char* file)
{
	mesh_list = 0;
	num_meshes = 0;
	
	loadFromFile(file);
}

bool Scene::loadFromFile(char* file)
{
	if (mesh_list != 0)
	{
		delete [] mesh_list;
		mesh_list = 0;
	}
	num_meshes = 0;
	
	
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile( file,
	aiProcess_CalcTangentSpace |
	aiProcess_Triangulate |
	aiProcess_JoinIdenticalVertices |
	aiProcess_GenSmoothNormals
	);

	if(!scene)
	{
		printf("Cannot load %s : %s\n", file, importer.GetErrorString());
		return false;
	}
	if (!scene->HasMeshes())
	{
		printf("No mesh in file %s\n", file);
		return false;
	}
	
	num_meshes = scene->mNumMeshes;
	mesh_list = new Mesh[num_meshes];
	
	printf("Loading %d meshes \n", num_meshes);
	
	for(unsigned int i=0; i<num_meshes; i++)
	{
		aiMesh* mai = scene->mMeshes[i];
		mesh_list[i].loadFromData(mai);
		
		scene_box.addBox(mesh_list[i].box());
	}
	
	return true;
}

void Scene::rotateOnMouse(GLWindow* w)
{
	for(unsigned int i=0; i<num_meshes; i++)
	{
		mesh_list[i].rotateOnMouse(w);
	}
}

