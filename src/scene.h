#ifndef SCENE_H_
#define SCENE_H_

#include "mesh.h"
#include "window.h"

#include <cstdio>

class Scene
{
private:
	Mesh* mesh_list;
	unsigned int num_meshes;
	
	BBox scene_box;
	
public:
	Scene();
	Scene(char* file);
	~Scene() { delete [] mesh_list; }
	
	bool loadFromFile(char* file);
	
	void rotateOnMouse(GLWindow* w);
	
	unsigned int getNumMeshes() { return num_meshes; }
	Mesh* getMeshesList() { return mesh_list; }
	
	BBox box() { return scene_box; }
};

#endif