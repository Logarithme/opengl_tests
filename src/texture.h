#ifndef TEXTURE_H_
#define TEXTURE_H_

#include "geo_math.h"

#include <GL/glew.h>
#include <GL/gl.h>
#include <algorithm>

#include <cstdio>

class Texture
{
    protected:
        GLuint texture_id;
        Vector3 dimensions;
        int channels;
        GLenum type;
        bool repeat;
    
    public:
        Texture() { glGenTextures(1, &texture_id); }
        unsigned int getId() { return texture_id; }
        
        unsigned int getChannels() { return channels; }
        
        virtual void activateTexture() {}
            
        virtual void generateMipmap() {}
        virtual void loadMipmapLevel(void* data, int level) {}
        
        virtual void attachToImageUnit(GLuint unit, GLenum access) { printf("texture attach to image unit not implemented yet\n"); }
};

class Texture1D : public Texture
{
    public:
        Texture1D();

        void loadTextureData(void* data, unsigned int w, GLenum type=GL_UNSIGNED_BYTE, int chan=3, bool repeat = 0, bool filter = 1);
	
	unsigned int getWidth() { return dimensions.x(); }
	
        virtual void generateMipmap();
        virtual void loadMipmapLevel(void* data, int level);
    
        virtual void activateTexture();
    
};

class Texture2D : public Texture
{
    public:
        Texture2D();
        Texture2D(const char* file, GLenum type=GL_UNSIGNED_BYTE, bool repeat = 0);
    
        void loadTexture(const char* file, GLenum type=GL_UNSIGNED_BYTE, bool repeat = 0);
        void loadTextureData(void* data, unsigned int w, unsigned int h, GLenum type=GL_UNSIGNED_BYTE, int chan=3, bool repeat = 0, bool filter = 1);
        //void loadEmptyTexture(unsigned int w, unsigned int h, GLenum type=GL_UNSIGNED_BYTE, int chan=3, bool repeat = 0);
	
        unsigned int getWidth() { return dimensions.x(); }
        unsigned int getHeight() { return dimensions.y(); }
	
        virtual void generateMipmap();
        virtual void loadMipmapLevel(void* data, int level);
    
        virtual void activateTexture();
		
		virtual void attachToImageUnit(GLuint unit, GLenum access);
    
};

class Texture3D : public Texture
{
    public:
        Texture3D();
        Texture3D(void* data, unsigned int w, unsigned int h, unsigned int d, GLenum type=GL_FLOAT, int chan=1, bool repeat=0);
    
        void loadTextureData(void* data, unsigned int w, unsigned int h, unsigned int d, GLenum type=GL_FLOAT, int chan=1, bool repeat=0,  bool filter = 1);
    
        virtual void generateMipmap();
        virtual void loadMipmapLevel(void* data, int level);
    
        virtual void activateTexture();
};

class SparseTexture3D : public Texture
{
    public:
        SparseTexture3D();
        SparseTexture3D(void* data, unsigned int w, unsigned int h, unsigned int d, GLenum type=GL_FLOAT, int chan=1, bool repeat=0);
    
        void loadTextureData(void* data, unsigned int w, unsigned int h, unsigned int d, GLenum type=GL_FLOAT, int chan=1, bool repeat=0, bool filter = 1);
    
        virtual void generateMipmap();
        virtual void loadMipmapLevel(void* data, int level);
    
        virtual void activateTexture();
};

class DepthTexture2D : public Texture
{
    public:
        DepthTexture2D() {}
    
        void loadEmptyTexture(unsigned int w, unsigned int h);
	
        unsigned int getWidth() { return dimensions.x(); }
        unsigned int getHeight() { return dimensions.y(); }
	
        virtual void generateMipmap();
        virtual void activateTexture();
};

#endif
