#include "texture.h"

#include <cstdio>
#include "geo_math.h"

/** Texture1D *******************************************************************/

Texture1D::Texture1D() : Texture() {}

void Texture1D::loadTextureData(void* data, unsigned int w, GLenum _type, int chan, bool _repeat, bool filter)
{
    dimensions.x(w);
    
    type = _type;
    channels = chan;
    
    repeat = _repeat;
    
    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_1D, texture_id);
    
    if (chan == 1)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage1D(GL_TEXTURE_1D, 0, GL_R8UI, w, 0, GL_RED, type, data);
        if (type == GL_FLOAT)
            glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, w, 0, GL_RED, type, data);
    }
    if (chan == 2)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage1D(GL_TEXTURE_1D, 0, GL_RG8UI, w, 0, GL_RG, type, data);
        if (type == GL_FLOAT)
            glTexImage1D(GL_TEXTURE_1D, 0, GL_RG32F, w, 0, GL_RG, type, data);
    }
    if (chan == 3)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, w, 0, GL_RGB, type, data);
        if (type == GL_FLOAT)
            glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB32F, w, 0, GL_RGB, type, data);
    }
    if (chan == 4)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, w, 0, GL_RGBA, type, data);
        if (type == GL_FLOAT)
            glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA32F, w, 0, GL_RGBA, type, data);
    }
    
    if (filter)
    {
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.f);
    }
    else
    {
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    
    if (repeat)
    {
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
}

void Texture1D::generateMipmap()
{
    glBindTexture(GL_TEXTURE_1D, texture_id);
    glGenerateMipmap(GL_TEXTURE_1D);
}

void Texture1D::loadMipmapLevel(void* data, int level)
{
    fprintf(stderr, "Texture1D::loadMipmapLevel not implemented yet\n");
}

void Texture1D::activateTexture()
{
    glActiveTexture(GL_TEXTURE0 + texture_id);
    glBindTexture(GL_TEXTURE_1D, texture_id);
}

