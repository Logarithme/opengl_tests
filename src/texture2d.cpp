#include "texture.h"

#include <cstdio>
#include "geo_math.h"
#include "CImg.h"

/** Texture2D *******************************************************************/

Texture2D::Texture2D() : Texture() {}
    
Texture2D::Texture2D(const char* file, GLenum type, bool repeat) : Texture()
{
    loadTexture(file, type, repeat);
}

void Texture2D::loadTexture(const char* file, GLenum type, bool repeat)
{
    printf("Loading %s as texture\n", file);
    
    int w=0;
    int h=0;
    int ch=0;
    
    cimg_library::CImg<unsigned char> image(file);
    w = image.width();
    h = image.height();
    ch = image.spectrum();
    
    unsigned char* data = (unsigned char*)malloc(sizeof(unsigned char)*w*h*ch);
    
    int indice = 0;
    for(int j=h-1; j>=0; j--)
    for(int i=0; i<w; i++)
    for(int c=0; c<ch; c++)
    {
	data[indice] = image(i, j, c); 
	indice++;
    }
    
    if (type == GL_FLOAT)
    {
	float* dataf = (float*)malloc(sizeof(float)*w*h*ch);
	for(int i=0; i<w*h*ch; i++)
		dataf[i] = data[i] / 255.0;
	loadTextureData(dataf, w, h, type, ch, repeat);
	free(dataf);
    }
    else
	loadTextureData(data, w, h, type, ch, repeat);
    
    free(data);
}

void Texture2D::loadTextureData(void* data, unsigned int w, unsigned int h, GLenum _type, int chan, bool _repeat, bool filter)
{
    dimensions.x(w);
    dimensions.y(h);
    
    type = _type;
    channels = chan;
    
    repeat = _repeat;
    
    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    
    if (chan == 1)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R8UI, w, h, 0, GL_RED, type, data);
        if (type == GL_FLOAT)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, w, h, 0, GL_RED, type, data);
    }
    if (chan == 2)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RG8UI, w, h, 0, GL_RG, type, data);
        if (type == GL_FLOAT)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, w, h, 0, GL_RG, type, data);
    }
    if (chan == 3)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, type, data);
        if (type == GL_FLOAT)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGB, type, data);
    }
    if (chan == 4)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, type, data);
        if (type == GL_FLOAT)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, w, h, 0, GL_RGBA, type, data);
    }
    
    if (filter)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.f);
    }
    else
    {
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    
    if (repeat)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
}

void Texture2D::attachToImageUnit(GLuint unit, GLenum access)
{
	GLenum format;
	if (channels == 1)
    {
		if (type == GL_UNSIGNED_BYTE)
			format =  GL_R8UI;
		if (type == GL_FLOAT)
			format =  GL_R32F;
    }
    if (channels == 2)
    {
        if (type == GL_UNSIGNED_BYTE)
			format =  GL_RG8UI;
        if (type == GL_FLOAT)
			format =  GL_RG32F;
    }
    if (channels == 3 || channels == 4)
    {
        if (type == GL_UNSIGNED_BYTE)
			format =  GL_RGBA;
        if (type == GL_FLOAT)
			format =  GL_RGBA32F;
    }
    
    glBindImageTexture(unit, texture_id, 0, GL_FALSE, 0, access, format);
}



/*void Texture2D::loadEmptyTexture(unsigned int w, unsigned int h, GLenum _type, int chan, bool _repeat)
{
    dimensions.x(w);
    dimensions.y(h);
    
    type = _type;
    channels = chan;
    
    repeat = _repeat;
    
    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    
    if (chan == 1)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R8UI, w, h, 0, GL_RED, type, NULL);
        if (type == GL_FLOAT)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, w, h, 0, GL_RED, type, NULL);
    }
    if (chan == 2)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RG8UI, w, h, 0, GL_RG, type, NULL);
        if (type == GL_FLOAT)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, w, h, 0, GL_RG, type, NULL);
    }
    if (chan == 3)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, type, NULL);
        if (type == GL_FLOAT)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGB, type, NULL);
    }
    if (chan == 4)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, type, NULL);
        if (type == GL_FLOAT)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, w, h, 0, GL_RGBA, type, NULL);
    }
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.f);
    
    if (repeat)
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
}*/

void Texture2D::generateMipmap()
{
    glBindTexture(GL_TEXTURE_2D, texture_id);
    glGenerateMipmap(GL_TEXTURE_2D);
}

void Texture2D::loadMipmapLevel(void* data, int level)
{
    fprintf(stderr, "Texture2D::loadMipmapLevel not implemented yet\n");
}

void Texture2D::activateTexture()
{
    glActiveTexture(GL_TEXTURE0 + texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);
}

