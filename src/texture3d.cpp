#include "texture.h"

#include <cstdio>
#include "geo_math.h"

/** Texture3D *******************************************************************/

Texture3D::Texture3D() : Texture() {}
    
Texture3D::Texture3D(void* data, unsigned int w, unsigned int h, unsigned int d, GLenum type, int chan, bool repeat) : Texture()
{
    loadTextureData(data, w, h, d, type, chan, repeat);
}

void Texture3D::loadTextureData(void* data, unsigned int w, unsigned int h, unsigned int d, GLenum _type, int chan, bool _repeat, bool filter)
{
    dimensions.x(w);
    dimensions.y(h);
    dimensions.z(d);
    
    type = _type;
    channels = chan;
    
    repeat = _repeat;
    
    glBindTexture(GL_TEXTURE_3D, texture_id);
    
    if (chan == 1)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage3D(GL_TEXTURE_3D, 0, GL_R8UI, w, h, d, 0, GL_RED, type, data);
        if (type == GL_FLOAT)
            glTexImage3D(GL_TEXTURE_3D, 0, GL_R32F, w, h, d, 0, GL_RED, type, data);
    }
    if (chan == 3)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, w, h, d, 0, GL_RGB, type, data);
        if (type == GL_FLOAT)
            glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB32F, w, h, d, 0, GL_RGB, type, data);
    }
    if (chan == 4)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, w, h, d, 0, GL_RGBA, type, data);
        if (type == GL_FLOAT)
            glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, w, h, d, 0, GL_RGBA, type, data);
    }

    /*
    if (chan == 1)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexStorage3D(GL_TEXTURE_3D, log2(std::min(w, std::min(h, d)))+1, GL_R8UI, w, h, d);
        if (type == GL_FLOAT)
            glTexStorage3D(GL_TEXTURE_3D, log2(std::min(w, std::min(h, d)))+1, GL_R32F, w, h, d);
        
        glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, w, h, d, GL_RED, type, data);
    }
    if (chan == 3)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexStorage3D(GL_TEXTURE_3D, log2(std::min(w, std::min(h, d)))+1, GL_RGB, w, h, d);
        if (type == GL_FLOAT)
            glTexStorage3D(GL_TEXTURE_3D, log2(std::min(w, std::min(h, d)))+1, GL_RGB32F, w, h, d);
        
         glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, w, h, d, GL_RGB, type, data);
    }
    if (chan == 4)
    {
        if (type == GL_UNSIGNED_BYTE)
            glTexStorage3D(GL_TEXTURE_3D, log2(std::min(w, std::min(h, d)))+1, GL_RGBA, w, h, d);
        if (type == GL_FLOAT)
            glTexStorage3D(GL_TEXTURE_3D, log2(std::min(w, std::min(h, d)))+1, GL_RGBA32F, w, h, d);
        
        glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, w, h, d, GL_RGBA, type, data);
    }
    */
    
    if (filter)
    {
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.f);
    }
    else
    {
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    
    if (repeat)
    {
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
}

void Texture3D::generateMipmap()
{
    glBindTexture(GL_TEXTURE_3D, texture_id);
    glGenerateMipmap(GL_TEXTURE_3D);
}

void Texture3D::loadMipmapLevel(void* data, int level)
{
    glBindTexture(GL_TEXTURE_3D, texture_id);

    Vector3 dim_level = dimensions * float(exp2(-level));
    dim_level.print();
    
    if (channels == 1)
        glTexSubImage3D(GL_TEXTURE_3D, level, 0, 0, 0, dim_level.x(), dim_level.y(), dim_level.z(), GL_RED, type, data);
    else if (channels == 3)
        glTexSubImage3D(GL_TEXTURE_3D, level, 0, 0, 0, dim_level.x(), dim_level.y(), dim_level.z(), GL_RGB, type, data);
    else if (channels == 4)
        glTexSubImage3D(GL_TEXTURE_3D, level, 0, 0, 0, dim_level.x(), dim_level.y(), dim_level.z(), GL_RGBA, type, data);
}

void Texture3D::activateTexture()
{
    glActiveTexture(GL_TEXTURE0 + texture_id);
    glBindTexture(GL_TEXTURE_3D, texture_id);
}
