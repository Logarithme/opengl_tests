#include "window.h"

#include <cstdio>

#ifndef _MSC_VER
  #define GK_CALLBACK
#else
  #define GK_CALLBACK __stdcall
#endif

static bool mousemove_glfw = false;

char keyOnce[GLFW_KEY_LAST + 1];
#define glfwGetKeyOnce(WINDOW, KEY)             \
    (glfwGetKey(WINDOW, KEY) ?              \
     (keyOnce[KEY] ? false : (keyOnce[KEY] = true)) :   \
     (keyOnce[KEY] = false))

void GK_CALLBACK AppDebug( GLenum source, GLenum type, unsigned int id, GLenum severity, GLsizei length, const char *message, void *userParam )
{
    if(severity == GL_DEBUG_SEVERITY_HIGH)
        printf("openGL error:\n%s\n", message);
    else if(severity == GL_DEBUG_SEVERITY_MEDIUM)
        printf("openGL warning:\n%s\n", message);
    else
        printf("openGL message:\n%s\n", message);
}

inline void TwEventMouseButtonGLFW3(GLFWwindow* window, int button, int action, int mods){TwEventMouseButtonGLFW(button, action);}

inline void TwEventMousePosGLFW3(GLFWwindow* window, double xpos, double ypos){mousemove_glfw = TwMouseMotion(int(xpos), int(ypos)); }

inline void TwEventMouseWheelGLFW3(GLFWwindow* window, double xoffset, double yoffset){TwEventMouseWheelGLFW(yoffset);}

inline void TwEventKeyGLFW3(GLFWwindow* window, int key, int scancode, int action, int mods){TwEventKeyGLFW(key, action);}

inline void TwEventCharGLFW3(GLFWwindow* window, int codepoint){TwEventCharGLFW(codepoint, GLFW_PRESS);}

GLWindow::GLWindow(bool debug, bool interface, int opengl_major, int opengl_minor, int size_x, int size_y, int MSAA, bool glew)
{
	open(debug, interface, opengl_major, opengl_minor, size_x, size_y, MSAA, glew);
}

// http://www.opengl-tutorial.org/beginners-tutorials/tutorial-1-opening-a-window/
int GLWindow::open(bool debug, bool interface, int opengl_major, int opengl_minor, int size_x, int size_y, int MSAA, bool glew)
{
	width = size_x;
	height = size_y;
	
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return -1;
    }
    
    glfwWindowHint(GLFW_SAMPLES, MSAA); // antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, opengl_major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, opengl_minor);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
     
    window = glfwCreateWindow( size_x, size_y, "opengl", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window\n" );
        glfwTerminate();
        return -1;
    }
    
    //glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    glfwMakeContextCurrent(window);
    
    if (glew)
    {
        glewExperimental=true; // Needed in core profile
        if (glewInit() != GLEW_OK) {
            fprintf(stderr, "Failed to initialize GLEW\n");
            return -1;
        }
        
        if(GLEW_ARB_debug_output && debug)
        {
            glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, 0, GL_TRUE);
            glDebugMessageCallbackARB(AppDebug, NULL);
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        }
    }
    
    TwInit(TW_OPENGL_CORE, NULL);
    TwWindowSize(size_x, size_y);
    
    last_cursor_x = -1;
    last_cursor_y = -1;
    
    glEnable(GL_DEPTH_TEST);
    
    projection = Perspective(45.0, (float)size_x/(float)size_y, 0.1, 1e6);

	anttweakbar = interface;
	if (anttweakbar)
	{
		glfwSetMouseButtonCallback(window, (GLFWmousebuttonfun)TwEventMouseButtonGLFW3);
		glfwSetCursorPosCallback(window, (GLFWcursorposfun)TwEventMousePosGLFW3);
		glfwSetScrollCallback(window, (GLFWscrollfun)TwEventMouseWheelGLFW3);
		glfwSetKeyCallback(window, (GLFWkeyfun)TwEventKeyGLFW3);
		glfwSetCharCallback(window, (GLFWcharfun)TwEventCharGLFW3);
	}
    
    return 0;
}

bool GLWindow::isClosed()
{
    return (glfwWindowShouldClose(window) != 0);
}

int mapQwertyAzerty(int q_code)
{
    if(q_code == GLFW_KEY_W) //asked key Azerty
        return GLFW_KEY_Z; //corresponding Qwerty Key
    if(q_code == GLFW_KEY_Z)
        return GLFW_KEY_W;
    if(q_code == GLFW_KEY_A)
        return GLFW_KEY_Q;
    if (q_code == GLFW_KEY_M)
        return GLFW_KEY_SEMICOLON;
    
    return q_code;
}

bool GLWindow::isKeyPressed(int keycode)
{   
    int azerty_code = mapQwertyAzerty(keycode);
    if( glfwGetKey(window, azerty_code) == GLFW_PRESS)
    {
        return true;
    }
    return false;
}

bool GLWindow::isKeyPressedOnce(int keycode)
{
    int azerty_code = mapQwertyAzerty(keycode);
    if( glfwGetKeyOnce(window, azerty_code) == GLFW_PRESS)
    {
        return true;
    }
    return false;
}

bool GLWindow::isClikedLeft()
{
    return (!mousemove_glfw && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS);
}

void GLWindow::getMouseMove(double* x, double* y)
{
    *x = current_cursor_x - last_cursor_x;
    *y = current_cursor_y - last_cursor_y;
}

void GLWindow::swapAndPoll()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
    if (anttweakbar)
        TwDraw();
    
    glfwSwapBuffers(window);
    glfwPollEvents();
    
    //Maintains move movement computation
    last_cursor_x = current_cursor_x;
    last_cursor_y = current_cursor_y;
    
    glfwGetCursorPos( window, &current_cursor_x, &current_cursor_y );

}
