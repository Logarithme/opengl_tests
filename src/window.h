#ifndef WINDOW_H_
#define WINDOW_H_

#include "geo_math.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <AntTweakBar.h>

class GLWindow
{
    GLFWwindow* window;
    
    double last_cursor_x;
    double last_cursor_y;
    double current_cursor_x;
    double current_cursor_y;
	
	int width;
	int height;
    
    Matrix44 projection;

    bool anttweakbar;
    
    public:
		GLWindow(bool debug=true, bool interface=false, int opengl_major=4, int opengl_minor=3, int size_x=1024, int size_y=768,  int MSAA=4, bool glew=true);
		
        int open(bool debug=true, bool interface=false, int opengl_major=4, int opengl_minor=3, int size_x=1024, int size_y=768,  int MSAA=4, bool glew=true);
    
        void swapAndPoll();
    
        bool isClosed(); //closed manually by user
        bool isKeyPressed(int keycode);
        bool isKeyPressedOnce(int keycode);
        bool isClikedLeft();
        void getMouseMove(double* x, double* y);
		
		int getWidth() { return width; }
		int getHeight() { return height; }
    
        GLFWwindow* getWindow() { return window; }
    
        Matrix44 getMatrix() { return projection; }
};

#endif
